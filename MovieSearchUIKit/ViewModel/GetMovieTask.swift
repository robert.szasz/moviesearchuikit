//
//  GetMoviesTask.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 26/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


class GetMovieTask {
    
    let movieId: Int
    let taskId: String
    unowned let parent: DetailViewModel
    var realmToken: NotificationToken?
    
    var movie: Movie? {
        didSet {
            realmToken?.invalidate()
            realmToken = nil
            parent.movieTaskCompleted(withTaskId: taskId)
        }
    }
    
    
    init(movieId: Int, taskId: String, parent: DetailViewModel) {
        self.movieId = movieId
        self.taskId = taskId
        self.parent = parent
        print("INITIALIZING MOVIE TASK")
    }
    
    public func getMovie() {
        saveInitialMovieStubToRealm(withMovieId: movieId)
        configureMovieToken(forMovie: movieId)
        
        var movieEndpoint = MovieEndpoint(ofKind: .MovieDetail)
        movieEndpoint.configureMovieDetailEndpoint(movieId: movieId)
        
        var movieAPI = MovieAPI()
        movieAPI.fetchMovieWithDetail(forEndpoint: movieEndpoint) { (result) in
            switch result {
            case .success(let movie):

                let moviesRealmOnDisk = RealmProvider.moviesOnDisk.realm
                do {
                    try moviesRealmOnDisk.write({
                        moviesRealmOnDisk.add(movie)
                    })
                } catch {
                    print("Tried to overwrite files.")
                }
            
                
                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    private func configureMovieToken(forMovie movieId: Int) {

         guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId) else { return }

         realmToken = movie.observe({ [weak self] change in
             switch change {
             case .change: self?.movie = movie
             case .error(let error): print("Error occurred: \(error)")
             case .deleted: print("Object was deleted")
             }
         })
     }
    
    private func saveInitialMovieStubToRealm(withMovieId movieId: Int) {
        let movieStub = Movie(id: movieId)
        let realmCollection = RealmProvider.moviesOnDisk.realm
        do {
            try realmCollection.write({
                realmCollection.add(movieStub, update: .all)
            })
        } catch {
            print("Tried to overwrite files.")
        }
    }
    
    
    deinit {
        print("DEINITIALIZING MOVIE TASK")
    }
}
