//
//  GenreLabel.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 18/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

class GenreLabel: UILabel {
    var insets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    
    override var textColor: UIColor! {
        get {
            return .white
        }
        
        set {
            super.textColor = .white
        }
    }
    
    override var font: UIFont! {
        get {
            return UIFont.systemFont(ofSize: 12, weight: .heavy)
        }
        set {
            super.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        }
    }
    
    override var backgroundColor: UIColor? {
        get {
            return .systemRed
        }
        set {
            super.backgroundColor = .systemRed
        }
    }
    
    override var numberOfLines: Int {
        get {
            return 1
        }
        set {
            super.numberOfLines = 1
        }
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + insets.left + insets.right
        let heigth = superContentSize.height + insets.top + insets.bottom
        return CGSize(width: width, height: heigth)
    }
}
