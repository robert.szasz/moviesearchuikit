//
//  RealmProvider.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 20/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


struct RealmProvider {
    
    private static let libraryDir = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first ?? ""
    
    let configuration: Realm.Configuration
    
    var realm: Realm {
        return try! Realm(configuration: configuration)
    }
    
    internal init(config: Realm.Configuration) {
        configuration = config
    }
    
    // MARK: - GENRES Realm on the disk
    private static func genresConfig(readOnly: Bool) -> Realm.Configuration {
        let libraryUrl = URL(fileURLWithPath: libraryDir)
        let libraryUrlWithRealm = libraryUrl.appendingPathComponent("genres.realm")
        
        return Realm.Configuration(fileURL: libraryUrlWithRealm, readOnly: readOnly, schemaVersion: 1, deleteRealmIfMigrationNeeded: !readOnly, objectTypes: [Genre.self])
    }
    
    public static var genresReadOnly: RealmProvider = {
        return RealmProvider(config: RealmProvider.genresConfig(readOnly: true))
    }()
    
    public static var genresReadWrite: RealmProvider = {
        return RealmProvider(config: RealmProvider.genresConfig(readOnly: false))
    }()
    
    // MARK: - COLLECTIONS Realm on the disk
    private static func collectionsOnDiskConfig() -> Realm.Configuration {
        let libraryUrl = URL(fileURLWithPath: libraryDir)
        let libraryUrlWithRealm = libraryUrl.appendingPathComponent("collections.realm")
        
        return Realm.Configuration(fileURL: libraryUrlWithRealm, schemaVersion: 1, deleteRealmIfMigrationNeeded: true, objectTypes: [Collection.self])
    }
    
    public static var collectionsOnDisk: RealmProvider = {
        return RealmProvider(config: RealmProvider.collectionsOnDiskConfig())
    }()
    
    // MARK: - MOVIES Realm on the disk
    private static func moviesOnDiskConfig() -> Realm.Configuration {
        let libraryUrl = URL(fileURLWithPath: libraryDir)
        let libraryUrlWithRealm = libraryUrl.appendingPathComponent("movies.realm")
        
        return Realm.Configuration(fileURL: libraryUrlWithRealm, schemaVersion: 1, deleteRealmIfMigrationNeeded: true, objectTypes: [Movie.self])
    }
    
    public static var moviesOnDisk: RealmProvider = {
        return RealmProvider(config: RealmProvider.moviesOnDiskConfig())
    }()
    
}
