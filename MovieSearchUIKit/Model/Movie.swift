//
//  Movie.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 17/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift

final class Movies: Codable {
    let results: [Movie]
    let total_pages: Int
    
    private enum StringKey: String, CodingKey {
        case results
        case total_pages
    }
    
    func save(toRealm realm: Realm = RealmProvider.moviesOnDisk.realm) {
        do {
            try realm.write({
                realm.add(self.results, update: .modified)
            })
        } catch {
            print("Movies saving to realm failed.")
        }
    }
}


class Movie: Object, Codable, Displayable {
    
    // MARK: - Properties
    // Attributes from the MovieDB main page
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var posterPath: String?
    @objc dynamic var backdropPath: String?
    @objc dynamic var popularity: Double = 0.0
    @objc dynamic var voteAverage: Double = 0.0
    @objc dynamic var voteCount: Int = 0
    @objc dynamic var overview: String = ""
    var genreIDs = List<Int>()

    // Attributes from the MovieDB detail page
    @objc dynamic var originalLanguage: String = ""
    @objc dynamic var status: String = ""
    let belongsToCollection = RealmOptional<Int>()
    
    @objc dynamic var posterThumbnailUrlOnDevice: String?
    @objc dynamic var posterw500UrlOnDevice: String?
    @objc dynamic var backdropw300UrlOnDevice: String?
    
    var posterThumbnailImage: UIImage?
    var posterw500Image: UIImage?
    var backdropw300Image: UIImage?
    
    // MARK: - Overridden methods
    override class func primaryKey() -> String? {
        return "id"
    }
    
    override class func indexedProperties() -> [String] {
        return ["posterPath"]
    }
    
    convenience init(id: Int) {
        self.init()
        
        self.id = id
    }
    
    // MARK: - Coding Keys
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case popularity

        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case genreIDs = "genre_ids"
    }
    
    // MARK: - (Static) methods
    enum PathType {
        case posterThumbnail
        case posterw500
        case backdropw300
    }
    
    @discardableResult
    func imageFromPathOnDevice(forPathType pathType: PathType) -> UIImage? {
        do {
            let appSupportDir = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            var url: URL
            
            switch pathType {
            case .posterThumbnail:
                url = appSupportDir.appendingPathComponent("/" + posterThumbnailUrlOnDevice!)
                let data = try Data(contentsOf: url)
                let image = UIImage(data: data)
                posterThumbnailImage = image
                return image
            case .posterw500:
                url = appSupportDir.appendingPathComponent("/" + posterw500UrlOnDevice!)
                let data = try Data(contentsOf: url)
                let image = UIImage(data: data)
                posterw500Image = image
                return image
            case .backdropw300:
                url = appSupportDir.appendingPathComponent("/" + backdropw300UrlOnDevice!)
                let data = try Data(contentsOf: url)
                let image = UIImage(data: data)
                backdropw300Image = image
                return image
            }
            
        } catch {
            print("Error occurred: \(error.localizedDescription)")
            return nil
        }
    }
}

struct MovieDetail: Codable {
    var originalLanguage: String
    var status: String
    var belongsToCollection: Int?
    
    private enum CodingKeys: String, CodingKey {
        case originalLanguage = "original_language"
        case status
        case belongsToCollection = "belongs_to_collection"
    }
    
    enum AdditionalIDKeys: String, CodingKey {
        case id
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        originalLanguage = try container.decode(String.self, forKey: .originalLanguage)
        status = try container.decode(String.self, forKey: .status)
        
        let belongsToCollectionContainer = try container.nestedContainer(keyedBy: AdditionalIDKeys.self, forKey: .belongsToCollection)
        belongsToCollection = try belongsToCollectionContainer.decode(Int?.self, forKey: .id)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(originalLanguage, forKey: .originalLanguage)
        try container.encode(status, forKey: .status)
        
        var belongsToCollectionContainer = container.nestedContainer(keyedBy: AdditionalIDKeys.self, forKey: .belongsToCollection)
        try belongsToCollectionContainer.encode(belongsToCollection, forKey: .id)
    }
    
}
