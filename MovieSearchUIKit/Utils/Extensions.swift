//
//  Extensions.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 11/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit


// MARK: UIView

extension UIView {
    
    func anchor(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, right: NSLayoutXAxisAnchor?, paddingTop: CGFloat, paddingLeft: CGFloat, paddingBottom: CGFloat, paddingRight: CGFloat, width: CGFloat, height: CGFloat) {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
        }
        
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: paddingRight).isActive = true
        }
        
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func dropRoundedShadow() {
        layoutIfNeeded()
        self.layer.cornerRadius = 15
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5
    }
    
    func addBlackGradientLayerInForeground(frame: CGRect, colors:[UIColor]) {
     let gradient = CAGradientLayer()
     gradient.frame = frame
     gradient.colors = colors.map{$0.cgColor}
     self.layer.addSublayer(gradient)
    }
}

// MARK: UIImageView

extension UIImageView {
    
    convenience init?(systemImage: String) {
        
        guard systemImage == "star.fill" || systemImage == "heart.circle.fill" else { return nil }
        guard let image = UIImage(systemName: systemImage) else { return nil }
    
        self.init(image: image)
        self.contentMode = .scaleAspectFit
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.frame = CGRect(x: 0, y: 0, width: 14, height: 14)
        
        switch systemImage {
        case "star.fill": self.tintColor = .systemYellow
        case "heart.circle.fill": self.tintColor = .systemRed
        default: return nil
        }
    }
}

// MARK: UILabel

extension UILabel {
    
    enum LabelType {
        case Rating
        case Like
    }
    
    convenience init(value: Float, forType type: LabelType) {
        
        self.init()
        self.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        switch type {
        case .Rating:
            self.textColor = .systemYellow
            self.text = "\(value)"
        case .Like:
            self.textColor = .systemRed
            self.text = "\(Int(value))"
        }
        
    }
}
