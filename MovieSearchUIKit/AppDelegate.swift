//
//  AppDelegate.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 11/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // print(NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first)
        
        var genresEndpoint = MovieEndpoint(ofKind: .Genre)
        genresEndpoint.configureGenreEndpoint()
        var movieAPI = MovieAPI()
        
        movieAPI.fetchFromMovieEndpoint(forEndpoint: genresEndpoint) { (result: Result<Genres, Error>) in
            
            switch result {
            case .failure(let error): print(error)
            case .success(let genresCollection):
                // print(genresCollection.genres)
                let genresRealm = RealmProvider.genresReadWrite.realm
            
                try! genresRealm.write {
                    genresRealm.deleteAll()
                    genresRealm.add(genresCollection.genres)
                }
            }
        }
                
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

