//
//  MovieListViewModelDelegateProtocol.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 20/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import UIKit

protocol MovieListViewModelDelegate: AnyObject {
    func handleSubscriptionToMoviesRealm()
    func handleUnsubscriptionToMoviesRealm()
    func handleReloadRow(atIndexPath indexPath: IndexPath)
}

protocol CollectionModelDelegate: AnyObject {
    var horizontalCollectionViewDataSource: [Displayable] { get set }
    var pinterestCollectionViewDataSource: [UIImage] { get set }
    var mainPosterPhoto: UIImageView { get set }
    
    func reloadHorizontalCollectionViewData()
    func reloadPinterestCollectionViewData()
}

protocol DetailViewModelDelegate: CollectionModelDelegate {
    var horizontalCVContainer: UIView { get }
    var horizontalCollectionView: UICollectionView { get }
    var horizontalCVContainerHeightAnchor: NSLayoutConstraint! { get set }
    var horizontalCollectionViewHeightAnchor: NSLayoutConstraint! { get set }
}

protocol CanGetImage: AnyObject {
    var basicImages: [GetImageTask] { get }
    
    func basicImageCompleted(withTaskId taskId: String)
}
