Welcome on the GitLab page of my first app - **MovieSearchUIKit**!\
(I know, the name sucks...)

![app-screenshot](MovieSearchUIKit_Screenshot.png "App screenshot")


[[_TOC_]]

## Overview
The MovieSearchUIKit is my first ever app which I made myself as a showcase for my skills/abilities - particularly with `UIKit` - in iOS Development.
It is by no means perfect or in a production state, some additional improvements are possible and much needed in the future, realization depends on my mood.
As far as I know, this is a bug-free semi-stable functioning version.\
The functionality was tested on the `iOS 13` OS.\
The layout was tested on an `iPhone 11 Pro Max`.

## Background
After 3 months from the first day I have written my first line of `Swift`, with a loose "learning phase" I thought I would be ready for my first internship to 
really push my learning curve.
Boyyyy I was wroooong!\
I applied to a Hungarian company called [**Supercharge**](https://supercharge.io), who were super kind and open to discuss a possible employment. As usual,
the first stage in the process would have been a - as I have some minimal experience now - a basic `homework`, which consisted of `networking` and some basic UI 
elements, like a `tableView`, `searchBar` and `tableViewCell`. I almost sh@!t my pants, as I got the homework. In my early days I thought I will be super cool, 
because I am going to learn `SwiftUI` instead of the good ol' pal `UIKit`, because that is what the cool kids are doing and I am going to be ahead of my time! (and 
because `UIKit`'s syntax and structure looked just hidious and super complicated :see_no_evil:) \
So I had to use the 19-fold of the given time interval just to be able to present something actually presentable. It was in `SwiftUI`, but the challenge stated, that 
it had to be compatible with at least `iOS 12`, so I failed at this miserable too. :poop: Feelings: \
\
![alt text](https://i.kym-cdn.com/entries/icons/original/000/028/021/work.jpg) \
\
Surprise, surprise, I **did not** get the job. \
**BUT** the company was very very very very very **kind** and **helpful**, so they gave me some positiv critic and advice for the future!\
I promised myself, that the first app I am ever going to build in `UIKit` has to be this one, so this is the result. But please remember, nontheless, this is 
*my first app*! 

## App logic
The app displays the movies associated with a search text in a `search bar`. Clicking on a movie shows a detail screen, which contains additional information about 
a particular movie, with some clickable `images` and an opportunity to navigate to an associated movie in a movie collection.


## Used frameworks/libraries
- Database library:
    - [x] `RealmSwift`

## Learned stuff (at least partially)
- First experience with `UIKit` and `AutoLayout`
- First experience with the `MVVM` architecture
- First experience with `API` calls and `networking`
- First experience with `Realm`
- First experience with creating the app **programmatically**
- Apple's `delegate` architecture
- `URLSession` & `URLSession delegate`
- Preventing retain cycles with `weak` & `unowned`
- Basics of Memory graph debugger

& *many more*...

## Side notes
1. **Populating table view with latest data at the app start**

  At every search, the data that set (movies) that is going to be displayed in the `tableView` is also saved to `realm`, 
  so that it is cahced and so the last search with the images can be shown at the restart of the app.
  
2. **Table view loads automatically new set of data when scrolled to the bottom**

  The `tableView` makes automatically the API call to fetch the next set of related data when the user scrolls to the bottom of it. It continues to do so 
  until the server does not have a next page to send.

3. **This code has parts from RayWenderlich sources**

  This code was written after I read the book [iOS Apprentice](https://store.raywenderlich.com/products/ios-apprentice) by the team of RayWenderlich,
  so it was my #1 inspiration for additional features and code solutions. My code can partially contain some RayWenderlich code snippets here and there.

4. **Maintenance? - No thanks!**

  I think the first project of an iOS developer has to be pure spaghetti code, because it really shows the struggle to get something done to a working state.
  Although at start I tried to apply some strict MVVM, later it loosened up and got more and more chaotic, just like a good *Italiano Spaghetti*! **Buon Appetito!**


## Goals for next project
- Basics of `reactive` programming
- Cleaner code architecture and maintainability `MVVM`
- More `UIKit` & `AutoLayout`
- More animations
- Support other `iOS` versions

## GIFs
The GIFs demonstrate the functionality of the app.\
Hover to see the functionality. \
(Sorry for the - partially - `potato` quality)

![detailscreen-navigating](MovieSearchUIKit-optimized.gif "Detail screen and navigation to other detail screens")

***

![tableView-loading](MovieSearchUIKit_Scrolling.gif "Scrolling to the bottom of the tableView loads automatically next set of data")

***

