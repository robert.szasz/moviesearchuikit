//
//  DetailViewController.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 17/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RealmSwift


fileprivate let reuseIdentifierCollectionCell = "collectionCell"
fileprivate let reuseIdentifierPinterestCell = "pinterestCell"

class DetailViewController: UIViewController {
    
    // MARK: - Properties
    
    var movie: Movie! {
        didSet {
            // Updating UI elements
            ratingLabel.text = String(movie.voteAverage)
            titleLabel.text = movie.title
            overviewLabel.text = movie.overview
            popularityLabel.text = String(movie.popularity)
        }
    }
    var detailViewModel: DetailViewModel!
    private var genreLabelSectionEnd: CGFloat = 0
    internal var horizontalCollectionViewDataSource = [Displayable]()
    internal var pinterestCollectionViewDataSource = [UIImage]()
    private var pinterestCollectionViewHeightAnchor: NSLayoutConstraint!
    var horizontalCollectionViewHeightAnchor: NSLayoutConstraint!
    var horizontalCVContainerHeightAnchor: NSLayoutConstraint!
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        sv.clipsToBounds = true
        sv.alwaysBounceVertical = true
        sv.contentInsetAdjustmentBehavior = .never
        return sv
    }()
    
    var mainPosterPhoto: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = UIImage(named: "default")
        return iv
    }()
    
    let starImageView: UIImageView = {
        guard let siv = UIImageView(systemImage: "star.fill") else { return UIImageView() }
        siv.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        return siv
    }()
    
    let ratingLabel: UILabel = {
        let label = UILabel(value: 8.2, forType: .Rating)
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 32, weight: .heavy)
        label.textColor = .white
        label.text = "The Intouchables"
        return label
    }()
    
    let statusLabel: UILabel = {
        let label = UILabel()
        label.text = "Status"
        return label
    }()
    
    let releasedLabel: UILabel = {
        let label = UILabel()
        label.text = "Released"
        return label
    }()
    
    let popularityTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Popularity"
        return label
    }()
    
    let popularityLabel: UILabel = {
        let label = UILabel()
        label.text = "11322"
        return label
    }()
    
    let languageTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Language"
        return label
    }()
    
    let languageLabel: UILabel = {
        let label = UILabel()
        label.text = "EN"
        return label
    }()
    
    let dividerView1: UIView = {
        let divider = UIView()
        divider.backgroundColor = .systemRed
        return divider
    }()
    
    let dividerView2: UIView = {
        let divider = UIView()
        divider.backgroundColor = .systemRed
        return divider
    }()
    
    let genreLabel: GenreLabel = {
        let gl = GenreLabel()
        gl.text = "Comedy"
        gl.layer.cornerRadius = 5
        gl.layer.masksToBounds = true
        return gl
    }()
    
    let overviewLabel: UILabel = {
        let label = UILabel()
        label.text = "Movie overview"
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    let horizontalCVContainer: UIView = {
        let container = UIView()
        container.backgroundColor = .white
        
        let label = UILabel()
        label.text = "Belongs to collection"
        label.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        label.textColor = .gray
        label.numberOfLines = 1
        container.addSubview(label)
        label.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let line = UIView()
        container.addSubview(line)
        line.anchor(top: nil, left: label.rightAnchor, bottom: nil, right: container.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 2)
        line.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        
        return container
    }()
    
    let pinterestCVContainer: UIView = {
        let container = UIView()
        container.backgroundColor = .white
        
        let label = UILabel()
        label.text = "Photos"
        label.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        label.textColor = .gray
        label.numberOfLines = 1
        container.addSubview(label)
        label.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let line = UIView()
        container.addSubview(line)
        line.anchor(top: nil, left: label.rightAnchor, bottom: nil, right: container.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 2)
        line.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        
        return container
    }()
    
    let horizontalCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.register(CollectionCell.self, forCellWithReuseIdentifier: reuseIdentifierCollectionCell)
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    let pinterestCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: PinterestLayout())
        cv.backgroundColor = .white
        cv.register(PinterestCell.self, forCellWithReuseIdentifier: reuseIdentifierPinterestCell)
        return cv
    }()
    
    lazy var titleAndRatingStack = makeTitleAndRatingStack()
    
    lazy var statusStack = makeDetailStack(withTitle: statusLabel, detail: releasedLabel)
    lazy var popularityStack = makeDetailStack(withTitle: popularityTitleLabel, detail: popularityLabel)
    lazy var languageStack = makeDetailStack(withTitle: languageTitleLabel, detail: languageLabel)
    

    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        configureBasicLabels()
        
        horizontalCollectionView.delegate = self
        horizontalCollectionView.dataSource = self
        
        pinterestCollectionView.delegate = self
        pinterestCollectionView.dataSource = self
        if let layout = pinterestCollectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        detailViewModel = DetailViewModel()
        detailViewModel.delegate = self
        
        view.addSubview(scrollView)
        scrollView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)        
        
        scrollView.addSubview(mainPosterPhoto)
        mainPosterPhoto.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 1.75)
        mainPosterPhoto.addBlackGradientLayerInForeground(frame: view.bounds, colors: [.clear, .black])
        
        mainPosterPhoto.addSubview(titleAndRatingStack)
        titleAndRatingStack.anchor(top: nil, left: mainPosterPhoto.leftAnchor, bottom: mainPosterPhoto.bottomAnchor, right: mainPosterPhoto.rightAnchor, paddingTop: 0, paddingLeft: bigPaddingOnTheLeft, paddingBottom: -12, paddingRight: -bigPaddingOnTheLeft, width: 0, height: 0)
        
        scrollView.addSubview(statusStack)
        statusStack.anchor(top: mainPosterPhoto.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 40, paddingLeft: bigPaddingOnTheLeft, paddingBottom: 0, paddingRight: 0, width: widthOfDetailStack, height: 0)
        
        scrollView.addSubview(dividerView1)
        dividerView1.anchor(top: statusStack.topAnchor, left: statusStack.rightAnchor, bottom: statusStack.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 1, height: 0)
        
        scrollView.addSubview(popularityStack)
        popularityStack.anchor(top: statusStack.topAnchor, left: dividerView1.rightAnchor, bottom: statusStack.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: widthOfDetailStack, height: 0)
        
        scrollView.addSubview(dividerView2)
        dividerView2.anchor(top: popularityStack.topAnchor, left: popularityStack.rightAnchor, bottom: popularityStack.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: 1, height: 0)
        
        scrollView.addSubview(languageStack)
        languageStack.anchor(top: popularityStack.topAnchor, left: dividerView2.rightAnchor, bottom: popularityStack.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: widthOfDetailStack, height: 0)
        
        configureGenreLabels()
        
        scrollView.addSubview(overviewLabel)
        overviewLabel.anchor(top: statusStack.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: genreLabelSectionEnd + bigPaddingOnTheLeft, paddingLeft: smallPaddingOnTheLeft, paddingBottom: 0, paddingRight: 0, width: UIScreen.main.bounds.width - 2 * smallPaddingOnTheLeft, height: 0)
        
        scrollView.addSubview(horizontalCVContainer)
        horizontalCVContainer.anchor(top: overviewLabel.bottomAnchor, left: overviewLabel.leftAnchor, bottom: nil, right: overviewLabel.rightAnchor, paddingTop: 32, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        horizontalCVContainer.layoutSubviews()
        horizontalCVContainer.layoutIfNeeded()
        let idealHeight = ceil(horizontalCVContainer.systemLayoutSizeFitting(CGSize(width: UIScreen.main.bounds.width, height: CGFloat.infinity)).height)
        horizontalCVContainerHeightAnchor = horizontalCVContainer.heightAnchor.constraint(equalToConstant: idealHeight)
        horizontalCVContainerHeightAnchor.isActive = true
        
        scrollView.addSubview(horizontalCollectionView)
        horizontalCollectionView.anchor(top: horizontalCVContainer.bottomAnchor, left: horizontalCVContainer.leftAnchor, bottom: nil, right: horizontalCVContainer.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        horizontalCollectionViewHeightAnchor = horizontalCollectionView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 3)
        horizontalCollectionViewHeightAnchor.isActive = true
        setLinesGradient(forContainerView: horizontalCVContainer)
        
        scrollView.addSubview(pinterestCVContainer)
        pinterestCVContainer.anchor(top: horizontalCollectionView.bottomAnchor, left: horizontalCollectionView.leftAnchor, bottom: nil, right: horizontalCollectionView.rightAnchor, paddingTop: bigPaddingOnTheLeft, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        setLinesGradient(forContainerView: pinterestCVContainer)
        
        scrollView.addSubview(pinterestCollectionView)

        pinterestCollectionView.anchor(top: pinterestCVContainer.bottomAnchor, left: pinterestCVContainer.leftAnchor, bottom: scrollView.bottomAnchor, right: pinterestCVContainer.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: -bigPaddingOnTheLeft, paddingRight: 0, width: 0, height: 0)
        pinterestCollectionView.layoutIfNeeded()
        let height = pinterestCollectionView.collectionViewLayout.collectionViewContentSize.height
        pinterestCollectionViewHeightAnchor = pinterestCollectionView.heightAnchor.constraint(equalToConstant: height)
        pinterestCollectionViewHeightAnchor.isActive = true
        
        detailViewModel.initializingViewController(withMovie: movie)
    }
    
    deinit {
        print("DEINITIALIZING DETAIL VIEW CONTROLLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }
    // MARK: - Helper methods
    
    func makeTitleAndRatingStack() -> UIStackView {

        let ratingStackView = UIStackView(arrangedSubviews: [starImageView, ratingLabel])
        ratingStackView.axis = .horizontal
        ratingStackView.spacing = 5
        
        let titleAndRatingStack = UIStackView(arrangedSubviews: [titleLabel, ratingStackView])
        titleAndRatingStack.axis = .vertical
        titleAndRatingStack.spacing = 20
        titleAndRatingStack.alignment = .leading
        
        return titleAndRatingStack
    }
    
    func makeDetailStack(withTitle titleView: UILabel, detail: UILabel) -> UIStackView {
        
        titleView.font = UIFont.systemFont(ofSize: 12)
        titleView.textColor = .darkGray
        
        detail.font = UIFont.systemFont(ofSize: 20)
        
        let stack = UIStackView(arrangedSubviews: [titleView, detail])
        stack.axis = .vertical
        stack.alignment = .leading
        stack.layoutIfNeeded()
        
        return stack
        
    }
    
    func configureGenreLabels() {
                
        let screenWidth = UIScreen.main.bounds.width
        let maxWritableWidth = screenWidth - smallPaddingOnTheLeft
        var previousLabelStart: CGFloat = smallPaddingOnTheLeft
        var previousLabelEnd:CGFloat = 0
        var rowCount: Int = 0
        let genresRealm = RealmProvider.genresReadOnly.realm
        
        for genreID in movie.genreIDs {
            let label = GenreLabel()
            label.text = genresRealm.object(ofType: Genre.self, forPrimaryKey: genreID)?.name
            label.layer.cornerRadius = 5
            label.layer.masksToBounds = true
            scrollView.addSubview(label)
            
            if previousLabelEnd + smallPaddingOnTheLeft + label.intrinsicContentSize.width < maxWritableWidth {
                
                label.anchor(top: statusStack.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 40 + CGFloat(rowCount) * (8 + label.intrinsicContentSize.height), paddingLeft: previousLabelEnd + smallPaddingOnTheLeft, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
                
                previousLabelStart = previousLabelEnd + smallPaddingOnTheLeft
                previousLabelEnd = previousLabelStart + label.intrinsicContentSize.width
                
                genreLabelSectionEnd = 40 + CGFloat(rowCount) * (8 + label.intrinsicContentSize.height) + label.intrinsicContentSize.height
                
            } else {
                
                rowCount += 1
                
                label.anchor(top: statusStack.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 40 + CGFloat(rowCount) * (8 + label.intrinsicContentSize.height), paddingLeft: smallPaddingOnTheLeft, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
                
                previousLabelStart = smallPaddingOnTheLeft
                previousLabelEnd = previousLabelStart + label.intrinsicContentSize.width
                
                genreLabelSectionEnd = 40 + CGFloat(rowCount) * (8 + label.intrinsicContentSize.height) + label.intrinsicContentSize.height
                
            }
            
        }
    }
    
    func setLinesGradient(forContainerView containerView: UIView) {
        containerView.layoutIfNeeded()
        let line = containerView.subviews[1]
        let gradient = CAGradientLayer()
        gradient.frame = line.bounds
        gradient.colors = [UIColor.gray.cgColor, UIColor.clear.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        line.layer.addSublayer(gradient)
    }
    
    func configureNavBar() {

        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithTransparentBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = .clear
        navigationItem.standardAppearance = navBarAppearance
        navigationItem.scrollEdgeAppearance = navBarAppearance
    }
    
    private func configureBasicLabels() {
        ratingLabel.text = String(movie.voteAverage)
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        popularityLabel.text = String(movie.popularity)
    }
}

// MARK: - Collection View Delegate
extension DetailViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, PinterestLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let image = pinterestCollectionViewDataSource[indexPath.item]
        if image.size.height > image.size.width {
            return 150
        } else {
            return 100
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == horizontalCollectionView {
            return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 3)
        }
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == horizontalCollectionView {
            return horizontalCollectionViewDataSource.count
        } else {
            return pinterestCollectionViewDataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == horizontalCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierCollectionCell, for: indexPath) as! CollectionCell
            // cell.collectionImageView.image = horizontalCollectionViewDataSource[indexPath.item]
            cell.item = horizontalCollectionViewDataSource[indexPath.item]
            return cell
        } else if collectionView == pinterestCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierPinterestCell, for: indexPath) as! PinterestCell
            cell.collectionImageView.image = pinterestCollectionViewDataSource[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == pinterestCollectionView {
            let selectedPhoto = pinterestCollectionViewDataSource[indexPath.item]
            
            guard let cellFrame = collectionView.layoutAttributesForItem(at: indexPath)?.frame, let nc = navigationController else { return }
            let originPoint = cellFrame.origin
                    
            print(collectionView.convert(originPoint, to: nc.view))
            
            let fullScreenView: UIImageView = {
                let iv = UIImageView(image: selectedPhoto)
                iv.contentMode = .scaleAspectFit
                iv.backgroundColor = .clear
                return iv
            }()
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            tapGestureRecognizer.numberOfTapsRequired = 1
            fullScreenView.isUserInteractionEnabled = true
            fullScreenView.addGestureRecognizer(tapGestureRecognizer)
            
            navigationController?.view.addSubview(fullScreenView)
            fullScreenView.frame = CGRect(origin: collectionView.convert(originPoint, to: nc.view), size: cellFrame.size)
            
            UIView.animate(withDuration: 0.4) {
                fullScreenView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: UIScreen.main.bounds.size)
                fullScreenView.backgroundColor = .white
            }
        } else if collectionView == horizontalCollectionView, let movie = horizontalCollectionViewDataSource[indexPath.item] as? Movie {
            if movie.id == self.movie.id {
                scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                return
            }
            
            let dest = DetailViewController()
            dest.movie = movie
            
            navigationController?.pushViewController(dest, animated: true)
        } else if collectionView == horizontalCollectionView, let collection = horizontalCollectionViewDataSource[indexPath.item] as? Collection {
            
            let dest = CollectionViewController()
            dest.collection = collection
            dest.horizontalCollectionViewDataSource = self.horizontalCollectionViewDataSource
            
            navigationController?.pushViewController(dest, animated: true)
        }
        
    }
    
    // MARK: - Handler
    
    @objc func imageTapped() {
        let fullScreenView = navigationController?.view.subviews.filter{ $0 is UIImageView }.first
        
        UIView.animate(withDuration: 0.4, animations: {
            fullScreenView?.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            fullScreenView?.alpha = 0
        }) { _ in
            fullScreenView?.removeFromSuperview()
        }
    }
}

// MARK: - ViewModel Delegate
extension DetailViewController: DetailViewModelDelegate {
    func reloadHorizontalCollectionViewData() {
        horizontalCollectionView.reloadData()
    }
    
    func reloadPinterestCollectionViewData() {
        pinterestCollectionView.reloadData()
        pinterestCollectionView.layoutIfNeeded()
        let height = pinterestCollectionView.collectionViewLayout.collectionViewContentSize.height
        pinterestCollectionViewHeightAnchor.constant = height
    }
}

