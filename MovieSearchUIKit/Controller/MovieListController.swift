//
//  MovieListController.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 11/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RealmSwift


fileprivate let reuseIdentifier = "MovieCell"
fileprivate let headerIdentifier = "SectionHeader"

class MovieListController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var movieListViewModel: MovieListViewModel!
    var movies = Array<Movie>()
    var token: NotificationToken?
    var hudView: LoadingHudView?
    
    let searchController: UISearchController = {
        let sc = UISearchController(searchResultsController: nil)
        sc.searchBar.placeholder = "Search for movies..."
        return sc
    }()
    
    let movieTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(MovieCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.register(MovieListHeader.self, forHeaderFooterViewReuseIdentifier: headerIdentifier)
        tableView.separatorColor = .clear
        return tableView
    }()
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        
        searchController.searchBar.delegate = self
        
        movieListViewModel = MovieListViewModel()
        movieListViewModel.delegate = self
        
        view.addSubview(movieTableView)
        movieTableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        movieTableView.dataSource = self
        movieTableView.delegate = self
    }


    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if movies.count != 0 {
            handleUnsubscriptionToMoviesRealm()
        }
        return movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MovieCell

        // Configure the cell...
        let movie = movies[indexPath.item]
        cell.movie = movie
        
        // 1. Check if UIImage is already in Realm model
        if let posterImage = movie.posterThumbnailImage {
            cell.photoImageView.image = posterImage
        }
        // 2. If not, check if Image is already downloaded and saved to Realm
        else if movie.posterThumbnailUrlOnDevice != nil {
            guard let image = movie.imageFromPathOnDevice(forPathType: .posterThumbnail) else { return cell }
            movies[indexPath.item].posterThumbnailImage = image
            cell.photoImageView.image = image
        }
        // 3. If not, start downloading image
        else {
            cell.photoImageView.image = #imageLiteral(resourceName: "default")
            movieListViewModel.fetchImages(forMovie: movie, forIndexPath: indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dest = DetailViewController()
        dest.movie = movies[indexPath.item]
        token?.invalidate()
        token = nil
        
        navigationController?.pushViewController(dest, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewCellHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! MovieListHeader
        return view
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == movies.count - 1 {
            movieListViewModel.loadMovies()
        }
    }
    
    // MARK: - Helper Methods
    
    func configureNavBar() {
        navigationItem.searchController = searchController
        navigationItem.title = "Movies"
        
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = .systemRed
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        searchController.searchBar.searchTextField.backgroundColor = .systemBackground
    }
    
    func updateSearchLabel(withText text: String) {
        UserDefaults.standard.set(text, forKey: searchTermString)
        UserDefaults.standard.synchronize()
        let myHeader = movieTableView.headerView(forSection: 0) as! MovieListHeader
        myHeader.searchTermLabel.text = UserDefaults.standard.string(forKey: searchTermString) ?? "No search found."
        myHeader.setNeedsDisplay()
    }
}

// MARK: - Search bar delegate

extension MovieListController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        hudView = LoadingHudView.loadingHud(inView: navigationController!.view)
        
        searchController.isActive = false
        updateSearchLabel(withText: text)
        
        movieListViewModel.userDidSearchForMovie(text)
    } 
}

// MARK: View Model delegate

extension MovieListController: MovieListViewModelDelegate {
    
    func handleSubscriptionToMoviesRealm() {
        token = RealmProvider.moviesOnDisk.realm.observe { [weak self] (notification, realm) in
            guard case .didChange = notification else { return }
            
            self?.movies = Array(realm.objects(Movie.self))
            self?.movieTableView.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
            self?.hudView?.hide()
            self?.hudView = nil
        }
    }
    
    func handleUnsubscriptionToMoviesRealm() {
        token?.invalidate()
        token = nil
    }
    
    func handleReloadRow(atIndexPath indexPath: IndexPath) {
        movieTableView.reloadRows(at: [indexPath], with: .none)
    }

}

// MARK: - Table View Prefetching
extension MovieListController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard movies[indexPath.item].posterThumbnailImage == nil else { return }
            
            let movie = movies[indexPath.item]
            
            if movie.posterThumbnailUrlOnDevice != nil {
                movie.imageFromPathOnDevice(forPathType: .posterThumbnail)
            } else {
                movieListViewModel.fetchImages(forMovie: movie, forIndexPath: indexPath)
            }
        }
    }
}
