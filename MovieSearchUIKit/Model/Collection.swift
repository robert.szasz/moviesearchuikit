//
//  Collection.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 20/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import RealmSwift


final class Collection: Object, Codable, Displayable {
    @objc dynamic var id: Int = 0
    @objc dynamic var posterPath: String = ""
    @objc dynamic var backdropPath: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var overview: String = ""
    var movieIDs = List<Int>()
    
    @objc dynamic var posterw500UrlOnDevice: String?
    @objc dynamic var backdropw300UrlOnDevice: String?
    
    var posterThumbnailImage: UIImage?
    var posterw500Image: UIImage?
    var backdropw300Image: UIImage?
    
    override class func primaryKey() -> String? {
        "id"
    }
    
    convenience init(id: Int, posterPath: String, backdropPath: String, movieIDs: List<Int>) {
        self.init()
        
        self.id = id
        self.posterPath = posterPath
        self.backdropPath = backdropPath
        self.movieIDs = movieIDs
    }
    
    convenience init(id: Int) {
        self.init()
        
        self.id = id
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
    }
    
    enum PathType {
        case posterw500
        case backdropw300
    }
    
    @discardableResult
    func imageFromPathOnDevice(forPathType pathType: PathType) -> UIImage? {
        do {
            let appSupportDir = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            var url: URL
            
            switch pathType {
            case .posterw500:
                url = appSupportDir.appendingPathComponent("/" + posterw500UrlOnDevice!)
                let data = try Data(contentsOf: url)
                let image = UIImage(data: data)
                posterw500Image = image
                return image
            case .backdropw300:
                url = appSupportDir.appendingPathComponent("/" + backdropw300UrlOnDevice!)
                let data = try Data(contentsOf: url)
                let image = UIImage(data: data)
                backdropw300Image = image
                return image
            }
            
        } catch {
            print("Error occurred: \(error.localizedDescription)")
            return nil
        }
    }
}
