//
//  CollectionCell.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 19/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    var item: Displayable? {
        didSet {
            label.text = item?.title
            
            if let movie = item as? Movie, let image = movie.posterThumbnailImage {
                collectionImageView.image = image
            } else if let collection = item as? Collection, let image = collection.posterw500Image {
                collectionImageView.image = image
            } else {
                return
            }
        }
    }

    
    let collectionImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 10
        iv.layer.borderWidth = 1
        iv.layer.borderColor = UIColor.lightGray.cgColor
        iv.image = UIImage(named: "default")
        return iv
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        addSubview(collectionImageView)
        collectionImageView.anchor(top: topAnchor, left: leftAnchor, bottom: label.topAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: -8, paddingRight: 0, width: 0, height: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol Displayable {
    var title: String { get set }
    var posterw500Image: UIImage? { get set }
    var posterThumbnailImage: UIImage? { get set }
}
