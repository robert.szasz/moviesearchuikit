//
//  GetCollectionTask.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 26/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


class GetCollectionTask {
    
    let collectionId: Int
    let taskId: String
    unowned let parent: DetailViewModel
    var realmToken: NotificationToken?
    
    var collection: Collection? {
        didSet {
            realmToken?.invalidate()
            realmToken = nil
            parent.collectionCompleted(withTaskId: taskId)
        }
    }
    
    
    init(collectionId: Int, taskId: String, parent: DetailViewModel) {
        self.collectionId = collectionId
        self.taskId = taskId
        self.parent = parent
        print("INITIALIZING COLLECTION DOWNLOAD TASK")
    }
    
    public func getCollection() {
        saveInitialCollectionStubToRealm(withCollectionId: collectionId)
        configureCollectionToken(forCollection: collectionId)
        
        var collectionEndpoint = MovieEndpoint(ofKind: .Collection)
        collectionEndpoint.configureCollectionEndpoint(collectionId: collectionId)

        var movieAPI = MovieAPI()
        movieAPI.fetchCollection(forEndpoint: collectionEndpoint) { [weak self] result in
            switch result {
            case .success(let collection):
                guard let self = self else { return }
                
                collection.id = self.collectionId
                let realmCollection = RealmProvider.collectionsOnDisk.realm
                do {
                    try realmCollection.write({
                        realmCollection.add(collection, update: .all)
                    })
                } catch {
                    print("Tried to overwrite files.")
                }
                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    private func configureCollectionToken(forCollection collectionId: Int) {

         guard let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: collectionId) else { return }

         realmToken = collection.observe({ [weak self] change in
             switch change {
             case .change: self?.collection = collection
             case .error(let error): print("Error occurred: \(error)")
             case .deleted: print("Object was deleted")
             }
         })
     }
    
    private func saveInitialCollectionStubToRealm(withCollectionId collectionId: Int) {
        let collectionStub = Collection(id: collectionId)
        let realmCollection = RealmProvider.collectionsOnDisk.realm
        do {
            try realmCollection.write({
                realmCollection.add(collectionStub, update: .all)
            })
        } catch {
            print("Tried to overwrite files.")
        }
    }
    
    
    deinit {
        print("DEINITIALIZING COLLECTION DOWNLOAD TASK")
    }
}
