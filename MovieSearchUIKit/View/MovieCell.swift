//
//  MovieCell.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 11/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    // MARK: Properties
    
    var movie: Movie! {
        didSet {
            configure(with: movie)
        }
    }
    
    let container: UIView = {
        let bv = UIView()
        bv.backgroundColor = .white
        bv.clipsToBounds = false
        return bv
    }()
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 10
        iv.layer.borderWidth = 1
        iv.layer.borderColor = UIColor.lightGray.cgColor
        iv.image = UIImage(named: "default")
        return iv
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 1
        label.text = "The Intouchables"
        return label
    }()
    
    let starImageView = UIImageView(systemImage: "star.fill")
    
    let ratingLabel = UILabel(value: 8.2, forType: .Rating)
    
    let heartImageView = UIImageView(systemImage: "heart.circle.fill")
    
    let likesLabel = UILabel(value: 11322, forType: .Like)
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = .darkGray
        label.text = "A true story of a man who sacrifices his life in order to help others, particularly a disabled older men, who has injured himself very very badly in an accident and as a consequence of this injury got into wheelchair."
        label.setContentHuggingPriority(.init(rawValue: 0.01), for: .vertical)
        label.numberOfLines = 0
        return label
    }()
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            frame.size.width = UIScreen.main.bounds.width
            super.frame = frame
        }
    }
    
    // MARK: Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .white
                
        addSubview(container)
        container.anchor(top: nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 30, paddingBottom: -10, paddingRight: -30, width: 0, height: cellHeight)
        container.dropRoundedShadow()
        
        container.addSubview(photoImageView)
        photoImageView.anchor(top: nil, left: container.leftAnchor, bottom: container.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 16, paddingBottom: -16, paddingRight: 0, width: imageViewWidth, height: imageViewHeight)
        
        let stack = generateOverallStack(withLabel: titleLabel, andDescription: descriptionLabel)
        container.addSubview(stack)
        stack.anchor(top: container.topAnchor, left: photoImageView.rightAnchor, bottom: photoImageView.bottomAnchor, right: container.rightAnchor, paddingTop: 8, paddingLeft: 16, paddingBottom: 0, paddingRight: -16, width: 0, height: 0)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Helper functions
    
    func generateStack() -> UIStackView {
        guard let starImageView = starImageView, let heartImageView = heartImageView else { return UIStackView() }
        
        let ratingStackView = UIStackView(arrangedSubviews: [starImageView, ratingLabel])
        ratingStackView.axis = .horizontal
        ratingStackView.spacing = 5
        
        let likeStackView = UIStackView(arrangedSubviews: [heartImageView, likesLabel])
        likeStackView.axis = .horizontal
        likeStackView.spacing = 5
        
        let stackView = UIStackView(arrangedSubviews: [ratingStackView, likeStackView])
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.alignment = .leading
        return stackView
        
    }
    
    func generateOverallStack(withLabel label: UILabel, andDescription description: UILabel) -> UIStackView {
                
        let stackView = UIStackView(arrangedSubviews: [label, generateStack(), description])
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fill
        stackView.spacing = 5
        
        return stackView
    }
    
    func configure(with movie: Movie) {
        titleLabel.text = movie.title
        ratingLabel.text = String(movie.voteAverage)
        likesLabel.text = String(movie.popularity)
        descriptionLabel.text = movie.overview
    }
}
