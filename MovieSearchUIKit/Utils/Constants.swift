//
//  Constants.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 11/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit


// MARK: - Movie List Controller
let cellHeight = CGFloat(130)
let imageViewHeight: CGFloat = 140
let imageViewWidth: CGFloat = 100

var tableViewCellHeight: CGFloat {
    get {
        return  imageViewHeight + 16 + 20
    }
}

// MARK: - Detail View Controller
let smallPaddingOnTheLeft: CGFloat = 12
let bigPaddingOnTheLeft: CGFloat = 20
var widthOfDetailStack: CGFloat {
    get {
        return ((UIScreen.main.bounds.width - 2 * bigPaddingOnTheLeft - 2 * 1 - 4 * 10) / 3).rounded(.down)
    }
}

// MARK: - User Defaults
let searchTermString = "SearchTerm"
let currentPageNumberString = "CurrentPageNumber"
let totalPageNumberString = "TotalPageNumber"

// MARK: - Other constants

public enum ImageFolderType: String {
    case ThumbnailImages
    case w300Images
    case w500Images
}
