//
//  CollectionViewController.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 02/05/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit
import RealmSwift


fileprivate let reuseIdentifierCollectionCell = "collectionCell"
fileprivate let reuseIdentifierPinterestCell = "pinterestCell"

class CollectionViewController: UIViewController {
    
    // MARK: - Properties
    
    var collection: Collection! {
        didSet {
            // Updating UI elements
            titleLabel.text = collection.title
            overviewLabel.text = collection.overview
            
            if let posterImage = collection.posterw500Image {
                pinterestCollectionViewDataSource.append(posterImage)
                mainPosterPhoto.image = posterImage
            } else {
                guard let image = collection.imageFromPathOnDevice(forPathType: .posterw500) else { return }
                pinterestCollectionViewDataSource.append(image)
                mainPosterPhoto.image = image
            }
            
            if let backdropImage = collection.backdropw300Image {
                pinterestCollectionViewDataSource.append(backdropImage)
            } else {
                guard let image = collection.imageFromPathOnDevice(forPathType: .backdropw300) else { return }
                pinterestCollectionViewDataSource.append(image)
            }
        }
    }

    internal var horizontalCollectionViewDataSource = [Displayable]()
    internal var pinterestCollectionViewDataSource = [UIImage]()
    private var pinterestCollectionViewHeightAnchor: NSLayoutConstraint!
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        sv.clipsToBounds = true
        sv.alwaysBounceVertical = true
        sv.contentInsetAdjustmentBehavior = .never
        return sv
    }()
    
    var mainPosterPhoto: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .white
        iv.image = UIImage(named: "default")
        return iv
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 32, weight: .heavy)
        label.textColor = .white
        label.text = "The Intouchables"
        return label
    }()
    
    let overviewLabel: UILabel = {
        let label = UILabel()
        label.text = "Movie overview"
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    let horizontalCVContainer: UIView = {
        let container = UIView()
        container.backgroundColor = .white
        
        let label = UILabel()
        label.text = "Belongs to collection"
        label.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        label.textColor = .gray
        label.numberOfLines = 1
        container.addSubview(label)
        label.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let line = UIView()
        container.addSubview(line)
        line.anchor(top: nil, left: label.rightAnchor, bottom: nil, right: container.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 2)
        line.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        
        return container
    }()
    
    let pinterestCVContainer: UIView = {
        let container = UIView()
        container.backgroundColor = .white
        
        let label = UILabel()
        label.text = "Photos"
        label.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        label.textColor = .gray
        label.numberOfLines = 1
        container.addSubview(label)
        label.anchor(top: container.topAnchor, left: container.leftAnchor, bottom: container.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let line = UIView()
        container.addSubview(line)
        line.anchor(top: nil, left: label.rightAnchor, bottom: nil, right: container.rightAnchor, paddingTop: 0, paddingLeft: 12, paddingBottom: 0, paddingRight: 0, width: 0, height: 2)
        line.centerYAnchor.constraint(equalTo: label.centerYAnchor).isActive = true
        
        return container
    }()
    
    let horizontalCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.register(CollectionCell.self, forCellWithReuseIdentifier: reuseIdentifierCollectionCell)
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    let pinterestCollectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: PinterestLayout())
        cv.backgroundColor = .white
        cv.register(PinterestCell.self, forCellWithReuseIdentifier: reuseIdentifierPinterestCell)
        return cv
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("WHY DOESNT IT PRINT THIS?")
        configureNavBar()
        configureBasicLabels()
        
        horizontalCollectionView.delegate = self
        horizontalCollectionView.dataSource = self
        
        pinterestCollectionView.delegate = self
        pinterestCollectionView.dataSource = self
        if let layout = pinterestCollectionView.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        view.addSubview(scrollView)
        scrollView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        scrollView.addSubview(mainPosterPhoto)
        mainPosterPhoto.anchor(top: scrollView.topAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 1.75)
        mainPosterPhoto.addBlackGradientLayerInForeground(frame: view.bounds, colors: [.clear, .black])
        
        mainPosterPhoto.addSubview(titleLabel)
        titleLabel.anchor(top: nil, left: mainPosterPhoto.leftAnchor, bottom: mainPosterPhoto.bottomAnchor, right: mainPosterPhoto.rightAnchor, paddingTop: 0, paddingLeft: bigPaddingOnTheLeft, paddingBottom: -12, paddingRight: -bigPaddingOnTheLeft, width: 0, height: 0)
        
        scrollView.addSubview(overviewLabel)
        overviewLabel.anchor(top: mainPosterPhoto.bottomAnchor, left: scrollView.leftAnchor, bottom: nil, right: nil, paddingTop: bigPaddingOnTheLeft, paddingLeft: smallPaddingOnTheLeft, paddingBottom: 0, paddingRight: 0, width: UIScreen.main.bounds.width - 2 * smallPaddingOnTheLeft, height: 0)
        
        scrollView.addSubview(horizontalCVContainer)
        horizontalCVContainer.anchor(top: overviewLabel.bottomAnchor, left: overviewLabel.leftAnchor, bottom: nil, right: overviewLabel.rightAnchor, paddingTop: 32, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        scrollView.addSubview(horizontalCollectionView)
        horizontalCollectionView.anchor(top: horizontalCVContainer.bottomAnchor, left: horizontalCVContainer.leftAnchor, bottom: nil, right: horizontalCVContainer.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: UIScreen.main.bounds.width / 3)
        setLinesGradient(forContainerView: horizontalCVContainer)
        
        scrollView.addSubview(pinterestCVContainer)
        pinterestCVContainer.anchor(top: horizontalCollectionView.bottomAnchor, left: horizontalCollectionView.leftAnchor, bottom: nil, right: horizontalCollectionView.rightAnchor, paddingTop: bigPaddingOnTheLeft, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        setLinesGradient(forContainerView: pinterestCVContainer)
        
        scrollView.addSubview(pinterestCollectionView)

        pinterestCollectionView.anchor(top: pinterestCVContainer.bottomAnchor, left: pinterestCVContainer.leftAnchor, bottom: scrollView.bottomAnchor, right: pinterestCVContainer.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: -bigPaddingOnTheLeft, paddingRight: 0, width: 0, height: 0)
        pinterestCollectionView.layoutIfNeeded()
        let height = pinterestCollectionView.collectionViewLayout.collectionViewContentSize.height
        pinterestCollectionViewHeightAnchor = pinterestCollectionView.heightAnchor.constraint(equalToConstant: height)
        pinterestCollectionViewHeightAnchor.isActive = true
        print("WHY DOESNT IT PRINT THIS?: ", height)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadPinterestCollectionViewData()
        reloadHorizontalCollectionViewData()
    }
    
    deinit {
        print("DEINITIALIZING COLLECTION VIEW CONTROLLER @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    }
    // MARK: - Helper methods
    
    func setLinesGradient(forContainerView containerView: UIView) {
        containerView.layoutIfNeeded()
        let line = containerView.subviews[1]
        let gradient = CAGradientLayer()
        gradient.frame = line.bounds
        gradient.colors = [UIColor.gray.cgColor, UIColor.clear.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        line.layer.addSublayer(gradient)
    }
    
    func configureNavBar() {

        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithTransparentBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.backgroundColor = .clear
        navigationItem.standardAppearance = navBarAppearance
        navigationItem.scrollEdgeAppearance = navBarAppearance
    }
    
    private func configureBasicLabels() {
        titleLabel.text = collection.title
        overviewLabel.text = collection.overview
    }
}

// MARK: - Collection View Delegate
extension CollectionViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, PinterestLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let image = pinterestCollectionViewDataSource[indexPath.item]
        if image.size.height > image.size.width {
            return 150
        } else {
            return 100
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == horizontalCollectionView {
            return CGSize(width: collectionView.frame.width / 4, height: collectionView.frame.width / 3)
        }
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == horizontalCollectionView {
            return horizontalCollectionViewDataSource.count
        } else {
            return pinterestCollectionViewDataSource.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == horizontalCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierCollectionCell, for: indexPath) as! CollectionCell
            cell.item = horizontalCollectionViewDataSource[indexPath.item]
            return cell
        } else if collectionView == pinterestCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierPinterestCell, for: indexPath) as! PinterestCell
            cell.collectionImageView.image = pinterestCollectionViewDataSource[indexPath.item]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == pinterestCollectionView {
            let selectedPhoto = pinterestCollectionViewDataSource[indexPath.item]
            
            guard let cellFrame = collectionView.layoutAttributesForItem(at: indexPath)?.frame, let nc = navigationController else { return }
            let originPoint = cellFrame.origin
                    
            print(collectionView.convert(originPoint, to: nc.view))
            
            let fullScreenView: UIImageView = {
                let iv = UIImageView(image: selectedPhoto)
                iv.contentMode = .scaleAspectFit
                iv.backgroundColor = .clear
                return iv
            }()
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
            tapGestureRecognizer.numberOfTapsRequired = 1
            fullScreenView.isUserInteractionEnabled = true
            fullScreenView.addGestureRecognizer(tapGestureRecognizer)
            
            navigationController?.view.addSubview(fullScreenView)
            fullScreenView.frame = CGRect(origin: collectionView.convert(originPoint, to: nc.view), size: cellFrame.size)
            
            UIView.animate(withDuration: 0.4) {
                fullScreenView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: UIScreen.main.bounds.size)
                fullScreenView.backgroundColor = .white
            }
        } else if collectionView == horizontalCollectionView, let movie = horizontalCollectionViewDataSource[indexPath.item] as? Movie {
            let dest = DetailViewController()
            dest.movie = movie
            
            navigationController?.pushViewController(dest, animated: true)
        }
        
    }
    
    // MARK: - Handler
    
    @objc func imageTapped() {
        let fullScreenView = navigationController?.view.subviews.filter{ $0 is UIImageView }.first
        
        UIView.animate(withDuration: 0.4, animations: {
            fullScreenView?.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            fullScreenView?.alpha = 0
        }) { _ in
            fullScreenView?.removeFromSuperview()
        }
    }
}

// MARK: - ViewModel Delegate
extension CollectionViewController: CollectionModelDelegate {
    func reloadHorizontalCollectionViewData() {
        horizontalCollectionView.reloadData()
    }
    
    func reloadPinterestCollectionViewData() {
        pinterestCollectionView.reloadData()
        pinterestCollectionView.layoutIfNeeded()
        let height = pinterestCollectionView.collectionViewLayout.collectionViewContentSize.height
        print("HEIGHT IS: ", height)
        pinterestCollectionViewHeightAnchor.constant = height
    }
}
