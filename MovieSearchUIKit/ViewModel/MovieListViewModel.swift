//
//  MovieListViewModel.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 20/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


class MovieListViewModel: NSObject, URLSessionDownloadDelegate, CanGetImage {
    
    weak var delegate: MovieListViewModelDelegate?
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        guard let imagePathURL = downloadTask.originalRequest?.url?.lastPathComponent else { return }
                        
        guard let appSupportDir = try? FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else { return }
                
        let dataPath = appSupportDir.appendingPathComponent(ImageFolderType.ThumbnailImages.rawValue, isDirectory: true)
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(at: dataPath, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        let savePath = dataPath.appendingPathComponent(imagePathURL)
        
        do {
            try FileManager.default.copyItem(at: location, to: savePath)
            let realmOnDisk = RealmProvider.moviesOnDisk.realm
            if let movie = realmOnDisk.objects(Movie.self).filter("posterPath = %@", "/" + imagePathURL).first {
                try realmOnDisk.write({
                    movie.posterThumbnailUrlOnDevice = "\(ImageFolderType.ThumbnailImages.rawValue)/" + imagePathURL
                })
            }
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
    }

// MARK: - View Controller supporting functions
    
    public func userDidSearchForMovie(_ movie: String) {

        // Unsubscribing to realm and cleaning up the last query results
        delegate?.handleUnsubscriptionToMoviesRealm()
        
        wipeCleanRealm(RealmProvider.moviesOnDisk.realm)
        wipeCleanRealm(RealmProvider.collectionsOnDisk.realm)
        wipeCleanImages(ofType: .ThumbnailImages)
        wipeCleanImages(ofType: .w300Images)
        wipeCleanImages(ofType: .w500Images)
        
        var movieEndpoint = MovieEndpoint(ofKind: .Movie)
        movieEndpoint.configureMovieEndpoint(query: movie)
        
        var movieAPI = MovieAPI()
        movieAPI.fetchFromMovieEndpoint(forEndpoint: movieEndpoint) { [weak self] (result: Result<Movies, Error>) in
            switch result {
            case .success(let movies):
                
                DispatchQueue.main.sync {
                    self?.delegate?.handleSubscriptionToMoviesRealm()
                }

                movies.save()
                
                UserDefaults.standard.set(1, forKey: currentPageNumberString)
                UserDefaults.standard.set(movies.total_pages, forKey: totalPageNumberString)
                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    public func loadMovies() {
        let userDefaults = UserDefaults.standard
        guard let searchTerm = userDefaults.string(forKey: searchTermString), userDefaults.integer(forKey: currentPageNumberString) != 0, userDefaults.integer(forKey: totalPageNumberString) != 0 else { return }
        
        let currentPageNumber = userDefaults.integer(forKey: currentPageNumberString)
        let totalPageNumber = userDefaults.integer(forKey: totalPageNumberString)
        
        guard currentPageNumber + 1 <= totalPageNumber else { return }
        
        var movieEndpoint = MovieEndpoint(ofKind: .Movie)
        movieEndpoint.configureMovieEndpoint(query: searchTerm, pageNumber: currentPageNumber + 1)
                
        var movieAPI = MovieAPI()
        movieAPI.fetchFromMovieEndpoint(forEndpoint: movieEndpoint) { [weak self] (result: Result<Movies, Error>) in
            switch result {
            case .success(let movies):
                
                DispatchQueue.main.sync {
                    self?.delegate?.handleSubscriptionToMoviesRealm()
                }

                movies.save()
                userDefaults.set(currentPageNumber + 1, forKey: currentPageNumberString)
                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - Image Task
        
    private(set) var basicImages = [GetImageTask]()
    
    func basicImageCompleted(withTaskId taskId: String) {
        guard basicImages.first(where: {$0.taskId == taskId}) != nil, let taskIdInt = Int(taskId) else { return }
        let indexPath = IndexPath(row: taskIdInt, section: 0)
        
        DispatchQueue.main.async { [weak self] in
            self?.delegate?.handleReloadRow(atIndexPath: indexPath)
        }
        
        basicImages.removeAll(where: {$0.taskId == taskId} )
    }
    
    public func fetchImages(forMovie movie: Movie, forIndexPath indexPath: IndexPath) {
        let id = movie.id
        let uid = String(indexPath.item)
        
        let imageTask = GetImageTask(objectId: id, taskId: uid, parent: self, objectType: .Movie, imageType: .Poster, imageSize: .Thumbnail)
        basicImages.append(imageTask)
        imageTask.getImage()
        
    }
    
    // MARK: - Helper Functions
    
    private func wipeCleanRealm(_ realm: Realm) {
        do {
            try realm.write({
                realm.deleteAll()
            })
        } catch {
            print("Error while deleting realm: \(error.localizedDescription)")
        }
    }
    
    private func wipeCleanImages(ofType type: ImageFolderType) {
        do {
            let fileManager = FileManager.default
            let thumbImagesFilePath = fileManager.urls(for: .applicationSupportDirectory, in: .userDomainMask).first!.appendingPathComponent(type.rawValue, isDirectory: true)
            let thumbImagesContent = try fileManager.contentsOfDirectory(at: thumbImagesFilePath, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            
            for filePath in thumbImagesContent {
                try fileManager.removeItem(at: filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    
}
