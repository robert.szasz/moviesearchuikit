//
//  Genre.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 20/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import RealmSwift

class Genres: Codable {
    let genres: [Genre]
    
    private enum StringKey: String, CodingKey {
        case genres
    }
}

final class Genre: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: Int, name: String) {
        self.init()
        
        self.id = id
        self.name = name
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name
    }
}
