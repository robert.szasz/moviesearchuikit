//
//  Network.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 13/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation


// MARK: - Protocol Endpoint
protocol Endpoint {
    var protocolType: String { get }
    var host: String { get }
    var path: String { get }
    var queryItems: [URLQueryItem] { get }
    var url: URL? { get }
}

extension Endpoint {
    var protocolType: String { return "https" }
}

// MARK: - MovieDB Endpoint
struct MovieEndpoint: Endpoint {
    var host: String = "api.themoviedb.org"
    var path: String = ""
    var queryItems = [URLQueryItem]()
    let kind: MoviePaths
    
    private let apiKey: String = API_KEY
    
    enum MoviePaths: String {
        case Movie = "/3/search/movie"
        case MovieDetail = "/3/movie/"
        case Collection = "/3/collection/"
        case Genre = "/3/genre/movie/list"
        
        func pathForMovieDetailOrCollection(withId id: Int) -> String {
            switch self {
            case .Movie, .Genre: return self.rawValue
            case .MovieDetail, .Collection: return (self.rawValue + String(id))
            }
        }
    }
    
    public var url: URL? {
        var components = URLComponents()
        components.scheme = protocolType
        components.host = host
        components.path = path
        components.queryItems = queryItems
        
        return components.url
    }
    
    init(ofKind kind: MoviePaths) {
        self.kind = kind
    }
    
    mutating func configureMovieEndpoint(language: String = "en-US", query: String, pageNumber: Int? = nil) {
        switch kind {
        case .Movie:
            queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                  URLQueryItem(name: "language", value: language),
                  URLQueryItem(name: "query", value: query)]
            
            if let pageNumber = pageNumber {
                queryItems += [URLQueryItem(name: "page", value: String(pageNumber))]
            }
            
            path = kind.rawValue
        default:
            return
        }
    }
    
    mutating func configureMovieDetailEndpoint(language: String = "en-US", movieId: Int) {
        switch kind {
        case .MovieDetail:
            queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                  URLQueryItem(name: "language", value: language)]
            
            path = kind.pathForMovieDetailOrCollection(withId: movieId)
        default:
            return
        }
    }
    
    mutating func configureCollectionEndpoint(language: String = "en-US", collectionId: Int) {
        switch kind {
        case .Collection:
            queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                  URLQueryItem(name: "language", value: language)]
            
            path = kind.pathForMovieDetailOrCollection(withId: collectionId)
        default:
            return
        }
    }
    
    mutating func configureGenreEndpoint(language: String = "en-US") {
        switch kind {
        case .Genre:
            queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                  URLQueryItem(name: "language", value: language)]
            
            path = kind.rawValue
        default:
            return
        }
    }
}

// MARK: - ImageEndpoint
struct ImageEndpoint: Endpoint {
    var host: String = "image.tmdb.org"
    var path: String = "/t/p/"
    var queryItems: [URLQueryItem] {
        return [URLQueryItem(name: "api_key", value: apiKey)]
    }
    let kind: ImageType
    
    private let apiKey: String = API_KEY
    
    enum BackdropSizes: String {
        case w300, w780, w1280, original
    }
    
    enum PosterSizes: String {
        case w92, w154, w185, w342, w500, w780, original
    }
    
    enum ImageType {
        case Poster
        case Backdrop
    }
    
    mutating func configurePoster(size: PosterSizes, forPosterPath posterPath: String) {
        switch kind {
        case .Poster:
            path = path + size.rawValue + posterPath
        default:
            return
        }
    }
    
    mutating func configureBackdrop(size: BackdropSizes, forBackdropPath backdropPath: String) {
        switch kind {
        case .Backdrop:
            path = path + size.rawValue + backdropPath
        default:
            return
        }
    }
    
    init(ofKind kind: ImageType) {
        self.kind = kind
    }
    
    public var url: URL? {
        var components = URLComponents()
        components.scheme = protocolType
        components.host = host
        components.path = path
        components.queryItems = queryItems
        
        return components.url
    }
    
}
