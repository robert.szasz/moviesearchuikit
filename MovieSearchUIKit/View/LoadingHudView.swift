//
//  LoadingHudView.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 29/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import UIKit

protocol Loadable {
    func showLoadingView()
    func hideLoadingView()
}

final class LoadingHudView: UIView {

    private let loadingString = "Loading..."
    private let colouredRectArray = [ColouredRect(color: .systemRed, frame: CGRect(x: 0, y: 0, width: 25, height: 15)),
                                     ColouredRect(color: .white, frame: CGRect(x: 0, y: 0, width: 25, height: 15)),
                                     ColouredRect(color: .systemRed, frame: CGRect(x: 0, y: 0, width: 25, height: 15)),
                                     ColouredRect(color: .white, frame: CGRect(x: 0, y: 0, width: 25, height: 15))]
    
    private let boxWidth: CGFloat = 200
    private let boxHeight: CGFloat = 200
    private let attribs = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.white]
    private lazy var estimatedSize = loadingString.size(withAttributes: attribs)
    private lazy var estimatedRoundedSize = CGSize(width: ceil(estimatedSize.width), height: ceil(estimatedSize.height))
    
    
    class func loadingHud(inView view: UIView) -> LoadingHudView {
        let hudView = LoadingHudView(frame: view.bounds)
        hudView.isOpaque = false
        
        view.addSubview(hudView)
        view.isUserInteractionEnabled = false
                
        return hudView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {

        
        let boxRect = CGRect(x: round((bounds.size.width - boxWidth) / 2),
                             y: round((bounds.size.height - boxHeight) / 2),
                             width: boxWidth,
                             height: boxHeight)
        
        let roundedRect = UIBezierPath(roundedRect: boxRect, cornerRadius: 10)
        UIColor(white: 0.3, alpha: 0.8).setFill()
        roundedRect.fill()
        

        
        let loadingRect = CGRect(x: round(center.x - (estimatedRoundedSize.width + 20) / 2),
                                 y: round(center.y - (estimatedRoundedSize.height + 20) / 2 + boxHeight / 4),
                                 width: estimatedRoundedSize.width + 20,
                                 height: estimatedRoundedSize.height + 20)
        
        let roundedLoadingRect = UIBezierPath(roundedRect: loadingRect, cornerRadius: 5)
        UIColor.systemRed.setFill()
        roundedLoadingRect.fill()

        let textPoint = CGPoint(x: center.x - estimatedRoundedSize.width / 2, y: center.y - estimatedRoundedSize.height / 2 + boxHeight / 4)
        loadingString.draw(at: textPoint, withAttributes: attribs)
    }
    
    private func setupView() {
        var centerXOffset: CGFloat = round((bounds.size.width - boxWidth) / 2 + 35)
        let subCalc: CGFloat = -(estimatedRoundedSize.height + 20) / 2
        let centerYOffset: CGFloat = subCalc + boxHeight / 4 - 24
                
        for rect in colouredRectArray {
            addSubview(rect)
            rect.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: centerXOffset, paddingBottom: 0, paddingRight: 0, width: rect.bounds.width, height: rect.bounds.height)
            rect.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: centerYOffset).isActive = true
            
            centerXOffset += rect.bounds.width + 10
        }
        
        animateRects()
    }
    
    private func animateRects() {
        var delayCounter = 0
        
        for rect in colouredRectArray {
            UIView.animate(withDuration: 1, delay: Double(delayCounter) * 0.1, options: [.repeat, .curveEaseInOut, .autoreverse], animations: {
                rect.transform = CGAffineTransform(translationX: 0, y: -70)
            }, completion: nil)
            delayCounter += 1
        }
    }
    
    func hide() {
        superview?.isUserInteractionEnabled = true
        removeFromSuperview()
    }
    
    deinit {
        print("DEINITIALIZING HUD VIEW")
    }

}

class ColouredRect: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init(color: UIColor, frame: CGRect) {
        self.init(frame: frame)
        
        backgroundColor = color
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
