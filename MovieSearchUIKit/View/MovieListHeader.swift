//
//  MovieListHeader.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 24/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//
import Foundation
import UIKit

class MovieListHeader: UITableViewHeaderFooterView {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.numberOfLines = 1
        label.text = "Your last search was:"
        return label
    }()
    
    let searchTermLabel: GenreLabel = {
        let gl = GenreLabel()
        gl.layer.cornerRadius = 5
        gl.layer.masksToBounds = true
        gl.text = UserDefaults.standard.string(forKey: "SearchTerm") ?? "No search found..."
        return gl
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .white
        
        contentView.addSubview(titleLabel)
        titleLabel.anchor(top: nil, left: contentView.leftAnchor, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 20, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        contentView.addSubview(searchTermLabel)
        searchTermLabel.anchor(top: contentView.topAnchor, left: titleLabel.rightAnchor, bottom: contentView.bottomAnchor, right: nil, paddingTop: 8, paddingLeft: 8, paddingBottom: -8, paddingRight: 0, width: 0, height: 0)
        searchTermLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
