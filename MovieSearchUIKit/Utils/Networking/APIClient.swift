//
//  APIClient.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 17/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift

protocol APIClient {
    var session: URLSession! { get }
    
    mutating func fetchFromMovieEndpoint<T: Codable>(forEndpoint endpoint: MovieEndpoint, completion: @escaping (Result<[T], Error>) -> Void)
    
    mutating func fetchFromImageEndpoint(forEndpoint endpoint: ImageEndpoint, withDelegate delegate: AnyObject)

}

enum APIError: String, Error {
    case unknown = "An unknown error occurred while networking."
    case badResponse = "There occurred an error while fetching data, the HTTP Status code is not in the range of 200 - 299."
    case jsonDecodingFailed = "The parsing of the JSON data to objects failed."
}


struct MovieAPI: APIClient {
    var session: URLSession!
    
    mutating func fetchFromMovieEndpoint<T: Codable>(forEndpoint endpoint: MovieEndpoint, completion: @escaping (Result<T, Error>) -> Void) {
        
        guard let url = endpoint.url else { return }
        session = URLSession.shared
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 5)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                completion(.failure(APIError.badResponse))
                return
            }

            guard let data = data, let movies = try? JSONDecoder().decode(T.self, from: data) else {
                completion(.failure(APIError.jsonDecodingFailed))
                return
            }
            
            completion(.success(movies))
        }
        task.resume()
    }
    
    
    mutating func fetchFromImageEndpoint(forEndpoint endpoint: ImageEndpoint, withDelegate delegate: AnyObject) {
        
        guard let url = endpoint.url else { return }
        guard let delegate = delegate as? URLSessionDownloadDelegate else { return }
        let configuration = URLSessionConfiguration.default
        
        session = URLSession(configuration: configuration, delegate: delegate, delegateQueue: nil)
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 5)
        
        let task = session.downloadTask(with: request)
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    mutating func fetchCollection(forEndpoint endpoint: MovieEndpoint, completion: @escaping (Result<Collection, Error>) -> Void) {
        guard let url = endpoint.url else { return }
        session = URLSession.shared
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 5)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                completion(.failure(APIError.badResponse))
                return
            }
            
            guard let data = data else {
                completion(.failure(APIError.jsonDecodingFailed))
                return
            }
            
            let collection = Collection()
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                guard let objects = json as? [String:Any], let posterPath = objects["poster_path"] as? String, let backdropPath = objects["backdrop_path"] as? String, let name = objects["name"] as? String, let overview = objects["overview"] as? String, let parts = objects["parts"] as? [[String:Any]] else { return }
                collection.posterPath = posterPath
                collection.backdropPath = backdropPath
                for movie in parts {
                    guard let movieID = movie["id"] as? Int else { return }
                    collection.movieIDs.append(movieID)
                }
                collection.title = name
                collection.overview = overview
                completion(.success(collection))
                return
            } catch {
                completion(.failure(APIError.jsonDecodingFailed))
            }
            
        }
        task.resume()
    }
    
    mutating func fetchMovieWithDetail(forEndpoint endpoint: MovieEndpoint, completion: @escaping (Result<Movie, Error>) -> Void) {
        guard let url = endpoint.url else { return }
        session = URLSession.shared
        guard case endpoint.kind = MovieEndpoint.MoviePaths.MovieDetail else { return }
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 5)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200..<300 ~= response.statusCode else {
                completion(.failure(APIError.badResponse))
                return
            }

            guard let data = data else {
                completion(.failure(APIError.jsonDecodingFailed))
                return
            }
            
            let movie = Movie()
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                
                guard let objects = json as? [String:Any], let backdropPath = objects["backdrop_path"] as? String, let belongsToCollection = objects["belongs_to_collection"] as? [String:Any], let collectionID = belongsToCollection["id"] as? Int, let genres = objects["genres"] as? [[String:Any]], let id = objects["id"] as? Int, let originalLanguage = objects["original_language"] as? String, let title = objects["title"] as? String, let overview = objects["overview"] as? String, let popularity = objects["popularity"] as? Double, let posterPath = objects["poster_path"] as? String, let status = objects["status"] as? String, let voteAverage = objects["vote_average"] as? Double, let voteCount = objects["vote_count"] as? Int else { return }
                movie.backdropPath = backdropPath
                movie.belongsToCollection.value = collectionID
                
                for genre in genres {
                    guard let id = genre["id"] as? Int else { continue }
                    movie.genreIDs.append(id)
                }
                
                movie.id = id
                movie.originalLanguage = originalLanguage
                movie.title = title
                movie.overview = overview
                movie.popularity = popularity
                movie.posterPath = posterPath
                movie.status = status
                movie.voteAverage = voteAverage
                movie.voteCount = voteCount
                completion(.success(movie))
                return
            } catch {
                completion(.failure(APIError.jsonDecodingFailed))
            }
        }
        task.resume()
    }
}
