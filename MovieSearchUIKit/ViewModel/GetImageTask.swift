//
//  getImageTask.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 25/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


class GetImageTask {
    
    enum ObjectType {
        case Movie
        case Collection
    }

    enum ImageType {
        case Poster
        case Backdrop
    }
    
    enum ImageSize {
        case Thumbnail
        case w300
        case w500
    }
    
    let objectId: Int
    let taskId: String
    unowned let parent: CanGetImage
    let objectType: ObjectType
    let imageType: ImageType
    let imageSize: ImageSize
    var realmToken: NotificationToken?
    
    var image: UIImage? {
        didSet {
            guard let image = image else { return }
            decideWhatToDoWithImage(image)
            realmToken?.invalidate()
            realmToken = nil
            decideWhatToNotify()
        }
    }
    
    private var isMovie: Bool {
        return objectType == ObjectType.Movie
    }
    
    
    init(objectId: Int, taskId: String, parent: CanGetImage, objectType: ObjectType, imageType: ImageType, imageSize: ImageSize = .w300) {
        self.objectId = objectId
        self.taskId = taskId
        self.parent = parent
        self.objectType = objectType
        self.imageType = imageType
        self.imageSize = imageSize
        print("INITIALIZING IMAGE TASK")
    }
    
    public func getImage() {
        let realm = isMovie ? RealmProvider.moviesOnDisk.realm : RealmProvider.collectionsOnDisk.realm
        let object = realm.object(ofType: isMovie ? Movie.self : Collection.self, forPrimaryKey: objectId)
                
        guard !(!isMovie && imageSize == .Thumbnail) else { return }
        
        if isMovie && imageSize == .Thumbnail {
            guard let object = object as? Movie else { return }
            
            if  let posterThumbnailImage = object.posterThumbnailImage {
                image = posterThumbnailImage
                return
            } else if object.posterThumbnailUrlOnDevice != nil {
                guard let posterThumbnailImage = object.imageFromPathOnDevice(forPathType: .posterThumbnail) else { return }
                image = posterThumbnailImage
                return
            } else {
                configurePosterThumbnailImageMovieToken(forMovie: objectId)
                fetchImages(forId: objectId, ofType: .Movie, .Poster, .Thumbnail)
            }
        }
        
        if isMovie && imageSize == .w500 {
            guard let object = object as? Movie else { return }
            
            if  let posterw500Image = object.posterw500Image {
                image = posterw500Image
                return
            } else if object.posterw500UrlOnDevice != nil {
                guard let posterw500Image = object.imageFromPathOnDevice(forPathType: .posterw500) else { return }
                image = posterw500Image
                return
            } else {
                configurePosterw500ImageMovieToken(forMovie: objectId)
                fetchImages(forId: objectId, ofType: .Movie, .Poster, .w500)
            }
        }
        
        if isMovie && imageType == .Backdrop {
            guard let object = object as? Movie else { return }
            
            if  let backdropw300Image = object.backdropw300Image {
                image = backdropw300Image
                return
            } else if object.backdropw300UrlOnDevice != nil {
                guard let backdropw300Image = object.imageFromPathOnDevice(forPathType: .backdropw300) else { return }
                image = backdropw300Image
                return
            } else {
                configureBackdropw300ImageMovieToken(forMovie: objectId)
                fetchImages(forId: objectId, ofType: .Movie, .Backdrop, .w300)
            }
        }
        
        if !isMovie && imageSize == .w500 {
            guard let object = object as? Collection else { return }
            
            if  let posterw500Image = object.posterw500Image {
                image = posterw500Image
                return
            } else if object.posterw500UrlOnDevice != nil {
                guard let posterw500Image = object.imageFromPathOnDevice(forPathType: .posterw500) else { return }
                image = posterw500Image
                return
            } else {
                configurePosterw500ImageCollectionToken(forCollection: objectId)
                fetchImages(forId: objectId, ofType: .Collection, .Poster, .w500)
            }
        }
        
        if !isMovie && imageType == .Backdrop {
            guard let object = object as? Collection else { return }
            
            if  let backdropw300Image = object.backdropw300Image {
                image = backdropw300Image
                return
            } else if object.backdropw300UrlOnDevice != nil {
                guard let backdropw300Image = object.imageFromPathOnDevice(forPathType: .backdropw300) else { return }
                image = backdropw300Image
                return
            } else {
                configureBackdropw300ImageCollectionToken(forCollection: objectId)
                fetchImages(forId: objectId, ofType: .Collection, .Backdrop, .w300)
            }
        }
    }
    
    // MARK: - Realm listeners
    
    private func configurePosterThumbnailImageMovieToken(forMovie movieId: Int) {

        guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId) else { return }

        realmToken = movie.observe({ [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "posterThumbnailUrlOnDevice":
                        guard property.newValue as? Optional<String> != nil, property.newValue as? String? != property.oldValue as? String? else { break }
                        guard let imageThumbnail = movie.imageFromPathOnDevice(forPathType: .posterThumbnail) else { break }
                        self?.image = imageThumbnail
                    default:
                        break
                    }
                }
            case .error(let error): print("Error occurred: \(error)")
            case .deleted: print("Object was deleted")
            }
        })
    }
    
    private func configurePosterw500ImageMovieToken(forMovie movieId: Int) {

        guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId) else { return }

        realmToken = movie.observe({ [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "posterw500UrlOnDevice":
                        guard property.newValue as? Optional<String> != nil, property.newValue as? String? != property.oldValue as? String? else { break }
                        guard let posterw500Image = movie.imageFromPathOnDevice(forPathType: .posterw500) else { break }
                        self?.image = posterw500Image
                    default:
                        break
                    }
                }
            case .error(let error): print("Error occurred: \(error)")
            case .deleted: print("Object was deleted")
            }
        })
    }
    
    private func configureBackdropw300ImageMovieToken(forMovie movieId: Int) {

        guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId) else { return }

        realmToken = movie.observe({ [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "backdropw300UrlOnDevice":
                        guard property.newValue as? Optional<String> != nil, property.newValue as? String? != property.oldValue as? String? else { break }
                        guard let backdropw300Image = movie.imageFromPathOnDevice(forPathType: .backdropw300) else { break }
                        self?.image = backdropw300Image
                    default:
                        break
                    }
                }
            case .error(let error): print("Error occurred: \(error)")
            case .deleted: print("Object was deleted")
            }
        })
    }
    
    private func configurePosterw500ImageCollectionToken(forCollection collectionId: Int) {

        guard let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: collectionId) else { return }

        realmToken = collection.observe({ [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "posterw500UrlOnDevice":
                        guard property.newValue as? Optional<String> != nil, property.newValue as? String? != property.oldValue as? String? else { break }
                        guard let posterw500Image = collection.imageFromPathOnDevice(forPathType: .posterw500) else { break }
                        self?.image = posterw500Image
                    default:
                        break
                    }
                }
            case .error(let error): print("Error occurred: \(error)")
            case .deleted: print("Object was deleted")
            }
        })
    }
    
    private func configureBackdropw300ImageCollectionToken(forCollection collectionId: Int) {

        guard let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: collectionId) else { return }

        realmToken = collection.observe({ [weak self] change in
            switch change {
            case .change(let properties):
                for property in properties {
                    switch property.name {
                    case "backdropw300UrlOnDevice":
                        guard property.newValue as? Optional<String> != nil, property.newValue as? String? != property.oldValue as? String? else { break }
                        guard let backdropw300Image = collection.imageFromPathOnDevice(forPathType: .backdropw300) else { break }
                        self?.image = backdropw300Image
                    default:
                        break
                    }
                }
            case .error(let error): print("Error occurred: \(error)")
            case .deleted: print("Object was deleted")
            }
        })
    }
    
    // MARK: - Parent notifying functions
    
    private func decideWhatToDoWithImage(_ image: UIImage) {
        switch parent {
        case let parent as DetailViewModel:
            if isMovie && (imageType == .Poster) && (imageSize == .w500) {
                parent.delegate?.pinterestCollectionViewDataSource.append(image)
                parent.delegate?.mainPosterPhoto.image = image
            } else if isMovie && (imageType == .Backdrop) {
                parent.delegate?.pinterestCollectionViewDataSource.append(image)
            } else if isMovie && (imageType == .Poster) && (imageSize == .Thumbnail) {
                print("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
                appendObjectToHorizontalDataSource(imageType: .Poster, imageSize: .Thumbnail, image: image)
            } else if !isMovie && imageType == .Poster && imageSize == .w500 {
                print("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH")
                appendObjectToHorizontalDataSource(imageType: .Poster, imageSize: .w500, image: image)
                parent.delegate?.pinterestCollectionViewDataSource.append(image)
            } else if !isMovie && imageType == .Backdrop {
                parent.delegate?.pinterestCollectionViewDataSource.append(image)
            }
        default:
            break
        }

    }
    
    private func decideWhatToNotify() {
        switch parent {
        case let parent as DetailViewModel:
            if isMovie && (imageType == .Poster) && (imageSize == .w500) {
                parent.basicImageCompleted(withTaskId: taskId)
            } else if isMovie && (imageType == .Backdrop) {
                parent.basicImageCompleted(withTaskId: taskId)
            } else if isMovie && (imageType == .Poster) && (imageSize == .Thumbnail) {
                parent.collectionMovieImageCompleted(withTaskId: taskId)
            } else if !isMovie && imageType == .Poster && imageSize == .w500 {
                parent.collectionImageCompleted(withTaskId: taskId)
            } else if !isMovie && imageType == .Backdrop {
                parent.collectionImageCompleted(withTaskId: taskId)
            }
        case let parent as MovieListViewModel:
            parent.basicImageCompleted(withTaskId: taskId)
        default:
            break
        }

    }
    
    // MARK: - Helper functions
    
    func appendObjectToHorizontalDataSource(imageType: ImageType, imageSize: ImageSize, image: UIImage) {

        guard let parent = parent as? DetailViewModel else { return }
        if isMovie && (imageType == .Poster) && (imageSize == .Thumbnail) {
            
            guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: objectId) else { return }
            movie.posterThumbnailImage = image
            parent.delegate?.horizontalCollectionViewDataSource.append(movie)
            print(parent.delegate?.horizontalCollectionViewDataSource.count)
            
        } else if !isMovie && imageType == .Poster && imageSize == .w500 {
            
            guard let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: objectId) else { return }
            collection.posterw500Image = image
            parent.delegate?.horizontalCollectionViewDataSource.append(collection)
            print(parent.delegate?.horizontalCollectionViewDataSource.count)

        } else {
            return
        }

        
    }
    
    // MARK: - Networking
    
    private func fetchImages(forId id: Int, ofType type: ObjectType, _ imageType: ImageType, _ imageSize: ImageSize) {
        let isMovie = (type == .Movie)
        let realm = isMovie ? RealmProvider.moviesOnDisk.realm : RealmProvider.collectionsOnDisk.realm
        
        guard let object = realm.object(ofType: isMovie ? Movie.self : Collection.self, forPrimaryKey: id) else { return }
        
        var imageEndpoint = ImageEndpoint(ofKind: .Poster)
        
        switch (object, imageType, imageSize) {
        case (let movie as Movie, .Poster, .Thumbnail):
            guard let imagePath = movie.posterPath else { break }
            imageEndpoint = ImageEndpoint(ofKind: .Poster)
            imageEndpoint.configurePoster(size: .w154, forPosterPath: imagePath)
        case (let movie as Movie, .Poster, .w500):
            guard let imagePath = movie.posterPath else { break }
            imageEndpoint = ImageEndpoint(ofKind: .Poster)
            imageEndpoint.configurePoster(size: .w500, forPosterPath: imagePath)
        case (let movie as Movie, .Backdrop, .w300):
            guard let imagePath = movie.backdropPath else { break }
            imageEndpoint = ImageEndpoint(ofKind: .Backdrop)
            imageEndpoint.configureBackdrop(size: .w300, forBackdropPath: imagePath)
        case (let collection as Collection, .Poster, .w500):
            let imagePath = collection.posterPath
            imageEndpoint = ImageEndpoint(ofKind: .Poster)
            imageEndpoint.configurePoster(size: .w500, forPosterPath: imagePath)
        case (let collection as Collection, .Backdrop, .w300):
            let imagePath = collection.backdropPath
            imageEndpoint = ImageEndpoint(ofKind: .Backdrop)
            imageEndpoint.configureBackdrop(size: .w300, forBackdropPath: imagePath)
        default:
            break
        }
        
        var imageAPI = MovieAPI()
        imageAPI.fetchFromImageEndpoint(forEndpoint: imageEndpoint, withDelegate: parent)
    }
    
    deinit {
        print("DEINITIALIZING IMAGE TASK")
    }
}
