//
//  DetailViewModel.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 23/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift


class DetailViewModel: NSObject, URLSessionDownloadDelegate, CanGetImage {
        
    weak var delegate: DetailViewModelDelegate?
    var getImageTask: GetImageTask?
    
    // MARK: - URLSession delegate
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let imagePathURL = downloadTask.originalRequest?.url?.lastPathComponent else { return }
        
        guard let path = downloadTask.originalRequest?.url?.deletingLastPathComponent().lastPathComponent, let appSupportDir = try? FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else { return }
        var dataPath: URL
        
        switch path {
        // Poster Images in big resolution
        case "w500":
            dataPath = appSupportDir.appendingPathComponent(ImageFolderType.w500Images.rawValue, isDirectory: true)
        // Backdrop Images in big resolution
        case "w300":
            dataPath = appSupportDir.appendingPathComponent(ImageFolderType.w300Images.rawValue, isDirectory: true)
        // Poster Images in thumbnail resolution
        case "w154":
            dataPath = appSupportDir.appendingPathComponent(ImageFolderType.ThumbnailImages.rawValue, isDirectory: true)
        default:
            dataPath = appSupportDir.appendingPathComponent(ImageFolderType.w500Images.rawValue, isDirectory: true)
        }
                                        
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do {
                try FileManager.default.createDirectory(at: dataPath, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        let savePath = dataPath.appendingPathComponent(imagePathURL)
        
        do {
            try FileManager.default.copyItem(at: location, to: savePath)
            
            // Create Movie and Collection realms
            let realmOnDisk = RealmProvider.moviesOnDisk.realm
            let collectionRealm = RealmProvider.collectionsOnDisk.realm

            switch dataPath.lastPathComponent {
            case ImageFolderType.w500Images.rawValue:
                
                if let movie = realmOnDisk.objects(Movie.self).filter("posterPath = %@", "/" + imagePathURL).first {
                    try realmOnDisk.write({
                        movie.posterw500UrlOnDevice = "\(ImageFolderType.w500Images.rawValue)/" + imagePathURL
                    })
                }
                
                if let collection = collectionRealm.objects(Collection.self).filter("posterPath = %@", "/" + imagePathURL).first {
                    try collectionRealm.write({
                        collection.posterw500UrlOnDevice = "\(ImageFolderType.w500Images.rawValue)/" + imagePathURL
                    })
                }
                
            case ImageFolderType.w300Images.rawValue:
                
                if let movie = realmOnDisk.objects(Movie.self).filter("backdropPath = %@", "/" + imagePathURL).first {
                    try realmOnDisk.write({
                        movie.backdropw300UrlOnDevice = "\(ImageFolderType.w300Images.rawValue)/" + imagePathURL
                    })
                }
                
                if let collection = collectionRealm.objects(Collection.self).filter("backdropPath = %@", "/" + imagePathURL).first {
                    try collectionRealm.write({
                        collection.backdropw300UrlOnDevice = "\(ImageFolderType.w300Images.rawValue)/" + imagePathURL
                    })
                }
                
            case ImageFolderType.ThumbnailImages.rawValue:
                
                if let movie = realmOnDisk.objects(Movie.self).filter("posterPath = %@", "/" + imagePathURL).first {
                    try realmOnDisk.write({
                        movie.posterThumbnailUrlOnDevice = "\(ImageFolderType.ThumbnailImages.rawValue)/" + imagePathURL
                    })
                }
                
            default:
                print("Did not find matching case. Error.")
                break
            }
            
            } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
    }

// MARK: - View Controller supporting functions
    
    public func fetchDetailInfo(forMovieId id: Int) {
        
        var movieEndpoint = MovieEndpoint(ofKind: .MovieDetail)
        movieEndpoint.configureMovieDetailEndpoint(movieId: id)
                
        var movieAPI = MovieAPI()
        movieAPI.fetchFromMovieEndpoint(forEndpoint: movieEndpoint) { (result: Result<MovieDetail, Error>) in
            switch result {
            case .success(let movieDetail):
                
                let realm = RealmProvider.moviesOnDisk.realm
                let movie = realm.object(ofType: Movie.self, forPrimaryKey: id)
                do {
                    try realm.write({
                        movie!.originalLanguage = movieDetail.originalLanguage
                        movie!.status = movieDetail.status
                        movie!.belongsToCollection.value = movieDetail.belongsToCollection
                    })
                } catch {
                    print("Error while trying to write movie details to movie to realm.")
                }
                                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    public func fetchCollectionInfo(withCollectionId collectionId: Int) {
        var collectionEndpoint = MovieEndpoint(ofKind: .Collection)
        collectionEndpoint.configureCollectionEndpoint(collectionId: collectionId)

        var movieAPI = MovieAPI()
        movieAPI.fetchCollection(forEndpoint: collectionEndpoint) { result in
            switch result {
            case .success(let collection):

                let realmCollection = RealmProvider.collectionsOnDisk.realm
                collection.id = collectionId
                do {
                    try realmCollection.write({
                        realmCollection.add(collection, update: .all)
                    })
                } catch {
                    print("Tried to overwrite files.")
                }
                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    public func downloadCollectionPoster(forCollection collection: ThreadSafeReference<Collection>) {
        let collectionRealm = RealmProvider.collectionsOnDisk.realm
        guard let collection = collectionRealm.resolve(collection) else { return }
        let posterPath = collection.posterPath
        
        var imageEndpoint = ImageEndpoint(ofKind: .Poster)
        imageEndpoint.configurePoster(size: .w500, forPosterPath: posterPath)

        var imageAPI = MovieAPI()
        imageAPI.fetchFromImageEndpoint(forEndpoint: imageEndpoint, withDelegate: self)
    }
    
    public func downloadCollectionBackdrop(forCollection collection: ThreadSafeReference<Collection>) {
        let collectionRealm = RealmProvider.collectionsOnDisk.realm
        guard let collection = collectionRealm.resolve(collection) else { return }
        let backdropPath = collection.backdropPath
        
        var imageEndpoint = ImageEndpoint(ofKind: .Backdrop)
        imageEndpoint.configureBackdrop(size: .w300, forBackdropPath: backdropPath)

        var imageAPI = MovieAPI()
        imageAPI.fetchFromImageEndpoint(forEndpoint: imageEndpoint, withDelegate: self)
    }
    
    public func makeImageFromUrlOnDevice(_ collection: ThreadSafeReference<Collection>) -> UIImage? {
        do {
            let collectionRealm = RealmProvider.collectionsOnDisk.realm
            guard let collection = collectionRealm.resolve(collection) else { return nil}
            let appSupportDir = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let url = appSupportDir.appendingPathComponent("/" + collection.posterw500UrlOnDevice!)
            let data = try Data(contentsOf: url)
            let image = UIImage(data: data)
            collection.posterw500Image = image
            return image
        } catch {
            print("Error occurred: \(error.localizedDescription)")
            return nil
        }
    }
    
    private func makeImageFromUrlOnDeviceMovie(forMovie movie: Movie) -> UIImage? {
        do {
            let appSupportDir = try! FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let url = appSupportDir.appendingPathComponent("/" + movie.posterThumbnailUrlOnDevice!)
            let data = try Data(contentsOf: url)
            let image = UIImage(data: data)
            return image
        } catch {
            print("Error occurred: \(error.localizedDescription)")
            return nil
        }
    }
    
    // MARK: - Task related code
    
    var id: Int = 0
    
    private(set) var basicImages = [GetImageTask]() {
        didSet {
            if basicImages.count == 0 {
                delegate?.reloadPinterestCollectionViewData()
            }
        }
    }
    
    private(set) var collectionImages = [GetImageTask]() {
        didSet {
            if collectionImages.count == 0 {
                delegate?.reloadHorizontalCollectionViewData()
                delegate?.reloadPinterestCollectionViewData()
            }
        }
    }
    
    private(set) var collectionMovieImages = [GetImageTask]() {
        didSet {
            if collectionMovieImages.count == 0 {
                delegate?.reloadHorizontalCollectionViewData()
            }
        }
    }
    
    private(set) var downloadCollectionTask = [GetCollectionTask]() {
        didSet {
            if downloadCollectionTask.count == 0 {
                guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: id), let collectionId = movie.belongsToCollection.value, let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: collectionId) else { return }
                initializeCollectionImages(withId: collection.id)

                var notExistentMovies = [Int]()
                for movieId in Array(collection.movieIDs) {
                    let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId)
                    if movie == nil { notExistentMovies.append(movieId) }
                }
                
                if notExistentMovies.count == 0 {
                    initializeCollectionMovieImages(withIds: Array(collection.movieIDs))
                    return
                }
                
                initializeNonExistentMovies(withIds: notExistentMovies)
            }
        }
    }
    
    private(set) var downloadMoviesTask = [GetMovieTask]() {
        didSet {
            if downloadMoviesTask.count == 0 {
                guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: id), let collectionId = movie.belongsToCollection.value, let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: collectionId) else { return }
                let movieIds = Array(collection.movieIDs)
                initializeCollectionMovieImages(withIds: movieIds)
            }
        }
    }
    
    private(set) var downloadMovieDetail: GetMovieDetail? {
        didSet {
            if downloadMovieDetail == nil {
                 guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: id) else { return }
                
                if movie.belongsToCollection.value == nil {
                    delegate?.horizontalCVContainerHeightAnchor.constant = 0
                    delegate?.horizontalCollectionViewHeightAnchor.constant = 0
                    delegate?.horizontalCVContainer.isHidden = true
                    delegate?.horizontalCollectionView.isHidden = true
                    delegate?.horizontalCVContainer.layoutIfNeeded()
                    delegate?.horizontalCollectionView.layoutIfNeeded()
                    return
                }
                
                let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: movie.belongsToCollection.value!)
                if collection == nil {
                    initializeCollection(withId: movie.belongsToCollection.value!)
                    return
                } else {
                    initializeCollectionImages(withId: collection!.id)
                                        
                    var notExistentMovies = [Int]()
                    for movieId in Array(collection!.movieIDs) {
                        let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId)
                        if movie == nil { notExistentMovies.append(movieId) }
                    }
                    
                    if notExistentMovies.count == 0 {
                        initializeCollectionMovieImages(withIds: Array(collection!.movieIDs))
                        return
                    }
                    
                    initializeNonExistentMovies(withIds: notExistentMovies)
                }
            }
        }
    }
    
    
    func basicImageCompleted(withTaskId taskId: String) {
        guard basicImages.first(where: {$0.taskId == taskId}) != nil else { return }
        
        basicImages.removeAll(where: {$0.taskId == taskId} )
    }
    
    func collectionImageCompleted(withTaskId taskId: String) {
        guard collectionImages.first(where: {$0.taskId == taskId}) != nil else { return }
        
        collectionImages.removeAll(where: {$0.taskId == taskId} )
    }
    
    func collectionMovieImageCompleted(withTaskId taskId: String) {
        guard collectionMovieImages.first(where: {$0.taskId == taskId}) != nil else { return }
        
        collectionMovieImages.removeAll(where: {$0.taskId == taskId} )
    }
    
    func collectionCompleted(withTaskId taskId: String) {
        guard downloadCollectionTask.first(where: {$0.taskId == taskId}) != nil else { return }
        
        downloadCollectionTask.removeAll(where: {$0.taskId == taskId} )
    }
    
    func movieTaskCompleted(withTaskId taskId: String) {
        guard downloadMoviesTask.first(where: {$0.taskId == taskId}) != nil else { return }
        
        downloadMoviesTask.removeAll(where: {$0.taskId == taskId} )
    }
    
    func movieDetailCompleted(forMovie movie: Movie) {
        downloadMovieDetail = nil
    }
    
    // MARK: - Initialize
    
    func initializingViewController(withMovie movie: Movie) {
        id = movie.id
        
        initializeBasicImages(withId: id)
        
        if movie.status == "" {
            initializeMovieDetail(withId: id)
            return
        }

        if movie.belongsToCollection.value == nil {
            delegate?.horizontalCVContainerHeightAnchor.constant = 0
            delegate?.horizontalCollectionViewHeightAnchor.constant = 0
            delegate?.horizontalCVContainer.isHidden = true
            delegate?.horizontalCollectionView.isHidden = true
            delegate?.horizontalCVContainer.layoutIfNeeded()
            delegate?.horizontalCollectionView.layoutIfNeeded()
            return
        }
        
        let collection = RealmProvider.collectionsOnDisk.realm.object(ofType: Collection.self, forPrimaryKey: movie.belongsToCollection.value!)
        if collection == nil {
            initializeCollection(withId: movie.belongsToCollection.value!)
            return
        }
        
        initializeCollectionImages(withId: collection!.id)
        
        var notExistentMovies = [Int]()
        for movieId in Array(collection!.movieIDs) {
            let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId)
            if movie == nil { notExistentMovies.append(movieId) }
        }
        
        if notExistentMovies.count == 0 {
            initializeCollectionMovieImages(withIds: Array(collection!.movieIDs))
            return
        }
        
        initializeNonExistentMovies(withIds: notExistentMovies)
   
    }
    
    func initializeBasicImages(withId id: Int) {
        let uid1 = UUID().uuidString
        let uid2 = UUID().uuidString
        
        let basicImageTaskArray = [GetImageTask(objectId: id, taskId: uid1, parent: self, objectType: .Movie, imageType: .Poster, imageSize: .w500), GetImageTask(objectId: id, taskId: uid2, parent: self, objectType: .Movie, imageType: .Backdrop)]
        basicImages.append(contentsOf: basicImageTaskArray)
        for task in basicImageTaskArray {
            task.getImage()
        }
    }
    
    func initializeMovieDetail(withId id: Int) {
        downloadMovieDetail = GetMovieDetail(movieId: id, parent: self)
        downloadMovieDetail?.getMovieDetail()
    }
    
    func initializeCollection(withId id: Int) {
        let uid = UUID().uuidString
        let collectionTask = GetCollectionTask(collectionId: id, taskId: uid, parent: self)
        downloadCollectionTask.append(collectionTask)
        collectionTask.getCollection()
    }
    
    func initializeCollectionImages(withId id: Int) {
        let uid1 = UUID().uuidString
        let uid2 = UUID().uuidString
        
        let collectionImageTaskArray = [GetImageTask(objectId: id, taskId: uid1, parent: self, objectType: .Collection, imageType: .Poster, imageSize: .w500), GetImageTask(objectId: id, taskId: uid2, parent: self, objectType: .Collection, imageType: .Backdrop)]
        collectionImages.append(contentsOf: collectionImageTaskArray)
        for task in collectionImageTaskArray {
            task.getImage()
        }
    }

    func initializeNonExistentMovies(withIds ids: [Int]) {
        
        for movieId in ids {
            let uid = UUID().uuidString
            
            DispatchQueue.global(qos: .default).async { [weak self] in
                guard let self = self else { return }
                let movieTask = GetMovieTask(movieId: movieId, taskId: uid, parent: self)
                self.downloadMoviesTask.append(movieTask)
                movieTask.getMovie()
            }
        }
    }
    
    func initializeCollectionMovieImages(withIds ids: [Int]) {
        
        for id in ids {
            let uid = UUID().uuidString
            let imageTask = GetImageTask(objectId: id, taskId: uid, parent: self, objectType: .Movie, imageType: .Poster, imageSize: .Thumbnail)
            collectionMovieImages.append(imageTask)
            imageTask.getImage()
        }
        
    }
    
    deinit {
        print("DEINITIALIZING DETAL VIEW MODEL ******************************")
    }

    
}
