//
//  GetMovieDetail.swift
//  MovieSearchUIKit
//
//  Created by Robert Szasz on 26/04/2020.
//  Copyright © 2020 Robert Szasz. All rights reserved.
//

import Foundation
import RealmSwift

class GetMovieDetail {
    
    let movieId: Int
    unowned let parent: DetailViewModel
    var realmToken: NotificationToken?
    
    var movie: Movie? {
        didSet {
            realmToken?.invalidate()
            realmToken = nil
            parent.movieDetailCompleted(forMovie: movie!)
        }
    }
    
    
    init(movieId: Int, parent: DetailViewModel) {
        self.movieId = movieId
        self.parent = parent
        print("INITIALIZING MOVIE DETAIL")
    }
    
    public func getMovieDetail() {
        configureMovieToken()
        
        var movieEndpoint = MovieEndpoint(ofKind: .MovieDetail)
        movieEndpoint.configureMovieDetailEndpoint(movieId: movieId)
                
        var movieAPI = MovieAPI()
        movieAPI.fetchFromMovieEndpoint(forEndpoint: movieEndpoint) { [weak self] (result: Result<MovieDetail, Error>) in
            switch result {
            case .success(let movieDetail):
                
                let realm = RealmProvider.moviesOnDisk.realm
                let movie = realm.object(ofType: Movie.self, forPrimaryKey: self?.movieId)
                do {
                    try realm.write({
                        movie!.originalLanguage = movieDetail.originalLanguage
                        movie!.status = movieDetail.status
                        movie!.belongsToCollection.value = movieDetail.belongsToCollection
                    })
                } catch {
                    print("Error while trying to write movie details to movie to realm.")
                }
                                
            case .failure(let error):
                print("Error. \(error.localizedDescription)")
            }
        }
    }
    
    private func configureMovieToken() {

         guard let movie = RealmProvider.moviesOnDisk.realm.object(ofType: Movie.self, forPrimaryKey: movieId) else { return }

         realmToken = movie.observe({ [weak self] change in
             switch change {
             case .change: self?.movie = movie
             case .error(let error): print("Error occurred: \(error)")
             case .deleted: print("Object was deleted")
             }
         })
     }

    deinit {
        print("DEINITIALIZING MOVIE DETAIL TASK")
    }
}
